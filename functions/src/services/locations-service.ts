import { FirebaseError } from 'firebase-admin';
import * as functions from 'firebase-functions';
import * as express from 'express';
import * as cors from 'cors';

import { firestore } from '../configs/db';
import { Location } from '@app/modules/locations/types/location';

const locationsCollection = firestore.collection('locations');

// init express server
const app = express();

// automatically allow cross-origin requests
app.use(cors({ origin: true }));

app.get('/', async (req, res) => {
  const { settingId, userId } = req.query;
  try {
    const locationsData = await locationsCollection
      .where('settingId', '==', parseInt(settingId as string, 10))
      .where('userId', '==', userId)
      .get()
      .then((querySnapshot) =>
        querySnapshot.docs.map(
          (doc) =>
            ({
              ...doc.data(),
              id: +doc.id,
            } as Location)
        )
      );

    res.send(locationsData);
  } catch (error) {
    res.send(new Error(JSON.stringify(error as FirebaseError)));
  }
});

app.get('/:id', async (req, res) => {
  const { id } = req.params;
  try {
    const locationData = await locationsCollection
      .doc(id)
      .get()
      .then((doc) => ({ ...doc.data(), id: +id } as Location));

    res.send(locationData);
  } catch (error) {
    res.send(new Error(JSON.stringify(error as FirebaseError)));
  }
});

app.post('', async (req, res) => {
  const {
    id,
    name,
    settingId,
    locationInfo,
    detailedLocationInfo,
    locationType,
    parentLocationId,
    userId,
  } = req.body as Location;

  try {
    await locationsCollection.doc(`${id}`).set({
      name,
      settingId,
      locationInfo,
      detailedLocationInfo,
      locationType,
      parentLocationId,
      userId,
    });

    res.send({ ...req.body } as Location);
  } catch (error) {
    res.send(new Error(JSON.stringify(error as FirebaseError)));
  }
});

app.put('/:id', async (req, res) => {
  const { id } = req.params;
  const {
    name,
    settingId,
    locationInfo,
    detailedLocationInfo,
    locationType,
    userId,
  } = req.body as Location;

  try {
    await locationsCollection.doc(id).update({
      name,
      settingId,
      locationInfo,
      detailedLocationInfo,
      locationType,
      userId,
    });

    res.send({ ...req.body, id: +id } as Location);
  } catch (error) {
    res.send(new Error(JSON.stringify(error as FirebaseError)));
  }
});

app.delete('/:id', async (req, res) => {
  const { id } = req.params;
  try {
    await locationsCollection.doc(id).delete();
    res.send();
  } catch (error) {
    res.send(new Error(JSON.stringify(error as FirebaseError)));
  }
});

export const locations = functions.https.onRequest(app);
