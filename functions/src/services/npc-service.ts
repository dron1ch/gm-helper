import { FirebaseError } from 'firebase-admin';
import * as functions from 'firebase-functions';
import * as express from 'express';
import * as cors from 'cors';

import { firestore } from '../configs/db';
import { Npc } from '@app/modules/npcs/types/npc';

const npcsCollection = firestore.collection('npcs');

// init express server
const app = express();

// automatically allow cross-origin requests
app.use(cors({ origin: true }));

app.get('/', async (req, res) => {
  const { settingId, userId } = req.query;

  try {
    const npcsData = await npcsCollection
      .where('settingId', '==', parseInt(settingId as string, 10))
      .where('userId', '==', userId)
      .get()
      .then((querySnapshot) =>
        querySnapshot.docs.map(
          (doc) =>
            ({
              ...doc.data(),
              id: +doc.id,
            } as Npc)
        )
      );

    res.send(npcsData);
  } catch (error) {
    res.send(new Error(JSON.stringify(error as FirebaseError)));
  }
});

app.get('/:id', async (req, res) => {
  const { id } = req.params;

  try {
    const npcData = await npcsCollection
      .doc(id)
      .get()
      .then((doc) => ({ ...doc.data(), id: +id } as Npc));

    res.send(npcData);
  } catch (error) {
    res.send(new Error(JSON.stringify(error as FirebaseError)));
  }
});

app.post('', async (req, res) => {
  const {
    id,
    name,
    settingId,
    description,
    parameters,
    shortDescription,
    src,
    userId,
  } = req.body as Npc;

  try {
    await npcsCollection.doc(`${id}`).set({
      name,
      settingId,
      src,
      description,
      parameters,
      shortDescription,
      userId,
    });

    res.send({ ...req.body } as Npc);
  } catch (error) {
    res.send(new Error(JSON.stringify(error as FirebaseError)));
  }
});

app.put('/:id', async (req, res) => {
  const { id } = req.params;
  const {
    name,
    settingId,
    src,
    description,
    parameters,
    shortDescription,
    userId,
  } = req.body as Npc;

  try {
    await npcsCollection.doc(id).update({
      name,
      settingId,
      description,
      parameters,
      shortDescription,
      src,
      userId,
    });

    res.send({ ...req.body, id: +id } as Npc);
  } catch (error) {
    res.send(new Error(JSON.stringify(error as FirebaseError)));
  }
});

app.delete('/:id', async (req, res) => {
  const { id } = req.params;

  try {
    await npcsCollection.doc(id).delete();

    res.send();
  } catch (error) {
    res.send(new Error(JSON.stringify(error as FirebaseError)));
  }
});

export const npcs = functions.https.onRequest(app);
