import { FirebaseError } from 'firebase-admin';
import * as functions from 'firebase-functions';
import * as express from 'express';
import * as cors from 'cors';

import { firestore } from '../configs/db';
import { Setting } from '@app/modules/settings/types/settings';

const settingsCollection = firestore.collection('settings');

// init express server
const app = express();

// automatically allow cross-origin requests
app.use(cors({ origin: true }));

app.get('/', async (req, res) => {
  const { userId } = req.query;

  try {
    const settingsData = await settingsCollection
      .where('userId', '==', userId)
      .get()
      .then((querySnapshot) =>
        querySnapshot.docs.map(
          (doc) =>
            ({
              ...doc.data(),
              id: +doc.id,
            } as Setting)
        )
      );

    res.send(settingsData);
  } catch (error) {
    res.send(new Error(JSON.stringify(error as FirebaseError)));
  }
});

app.get('/:id', async (req, res) => {
  const { id } = req.params;

  try {
    const settingData = await settingsCollection
      .doc(id)
      .get()
      .then((doc) => ({ ...doc.data(), id: +id } as Setting));

    res.send(settingData);
  } catch (error) {
    res.send(new Error(JSON.stringify(error as FirebaseError)));
  }
});

app.post('', async (req, res) => {
  const { id, name, userId } = req.body as Setting;

  try {
    await settingsCollection.doc(`${id}`).set({
      name,
      userId,
    });

    res.send({ ...req.body } as Setting);
  } catch (error) {
    res.send(new Error(JSON.stringify(error as FirebaseError)));
  }
});

app.put('/:id', async (req, res) => {
  const { id } = req.params;

  const { name, userId } = req.body as Setting;

  try {
    await settingsCollection.doc(id).update({
      name,
      userId,
    });

    res.send({ ...req.body, id: +id } as Setting);
  } catch (error) {
    res.send(new Error(JSON.stringify(error as FirebaseError)));
  }
});

app.delete('/:id', async (req, res) => {
  const { id } = req.params;

  try {
    await settingsCollection.doc(id).delete();

    res.send();
  } catch (error) {
    res.send(new Error(JSON.stringify(error as FirebaseError)));
  }
});

export const settings = functions.https.onRequest(app);
