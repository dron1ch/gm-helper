import { FirebaseError } from 'firebase-admin';
import * as functions from 'firebase-functions';
import * as express from 'express';
import * as cors from 'cors';

import { firestore } from '../configs/db';
import { Map } from '@app/modules/maps/types/map';

const mapsCollection = firestore.collection('maps');

// init express server
const app = express();

// automatically allow cross-origin requests
app.use(cors({ origin: true }));

app.get('/', async (req, res) => {
  const { settingId, userId } = req.query;

  try {
    const mapsData = await mapsCollection
      .where('settingId', '==', parseInt(settingId as string, 10))
      .where('userId', '==', userId)
      .get()
      .then((querySnapshot) =>
        querySnapshot.docs.map(
          (doc) =>
            ({
              ...doc.data(),
              id: +doc.id,
            } as Map)
        )
      );

    res.send(mapsData);
  } catch (error) {
    res.send(new Error(JSON.stringify(error as FirebaseError)));
  }
});

app.get('/:id', async (req, res) => {
  const { id } = req.params;

  try {
    const mapData = await mapsCollection
      .doc(id)
      .get()
      .then((doc) => ({ ...doc.data(), id: +id } as Map));

    res.send(mapData);
  } catch (error) {
    res.send(new Error(JSON.stringify(error as FirebaseError)));
  }
});

app.post('', async (req, res) => {
  const { id, name, settingId, markers, src, userId } = req.body as Map;

  try {
    await mapsCollection.doc(`${id}`).set({
      name,
      settingId,
      markers,
      src,
      userId,
    });

    res.send({ ...req.body } as Map);
  } catch (error) {
    res.send(new Error(JSON.stringify(error as FirebaseError)));
  }
});

app.put('/:id', async (req, res) => {
  const { id } = req.params;
  const { name, settingId, markers, src } = req.body as Map;

  try {
    await mapsCollection.doc(id).update({
      name,
      settingId,
      markers,
      src,
    });

    res.send({ ...req.body, id: +id } as Map);
  } catch (error) {
    res.send(new Error(JSON.stringify(error as FirebaseError)));
  }
});

app.delete('/:id', async (req, res) => {
  const { id } = req.params;

  try {
    await mapsCollection.doc(id).delete();

    res.send();
  } catch (error) {
    res.send(new Error(JSON.stringify(error as FirebaseError)));
  }
});

export const maps = functions.https.onRequest(app);
