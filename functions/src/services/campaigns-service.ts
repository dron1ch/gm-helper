import { FirebaseError } from 'firebase-admin';
import * as functions from 'firebase-functions';
import * as express from 'express';
import * as cors from 'cors';

import { firestore } from '../configs/db';
import { Campaign } from '@app/modules/campaigns/types/campaigns';

const campaignsCollection = firestore.collection('campaigns');

// init express server
const app = express();

// automatically allow cross-origin requests
app.use(cors({ origin: true }));

app.get('/', async (req, res) => {
  const { settingId, userId } = req.query;
  try {
    const campaignsData = await campaignsCollection
      .where('settingId', '==', parseInt(settingId as string, 10))
      .where('userId', '==', userId)
      .get()
      .then((querySnapshot) =>
        querySnapshot.docs.map(
          (doc) =>
            ({
              ...doc.data(),
              id: +doc.id,
            } as Campaign)
        )
      );

    res.send(campaignsData);
  } catch (error) {
    res.send(new Error(JSON.stringify(error as FirebaseError)));
  }
});

app.get('/:id', async (req, res) => {
  const { id } = req.params;

  try {
    const campaignData = await campaignsCollection
      .doc(id)
      .get()
      .then((doc) => ({ ...doc.data(), id: +id } as Campaign));

    res.send(campaignData);
  } catch (error) {
    res.send(new Error(JSON.stringify(error as FirebaseError)));
  }
});

app.post('', async (req, res) => {
  const { id, name, settingId, chapters, userId } = req.body as Campaign;

  try {
    await campaignsCollection.doc(`${id}`).set({
      name,
      settingId,
      chapters,
      userId,
    });

    res.send({ ...req.body } as Campaign);
  } catch (error) {
    res.send(new Error(JSON.stringify(error as FirebaseError)));
  }
});

app.put('/:id', async (req, res) => {
  const { id } = req.params;
  const { name, settingId, chapters, userId } = req.body as Campaign;
  try {
    await campaignsCollection.doc(id).update({
      name,
      settingId,
      chapters,
      userId,
    });

    res.send({ ...req.body, id: +id } as Campaign);
  } catch (error) {
    res.send(new Error(JSON.stringify(error as FirebaseError)));
  }
});

app.delete('/:id', async (req, res) => {
  const { id } = req.params;
  try {
    await campaignsCollection.doc(id).delete();
    res.send();
  } catch (error) {
    res.send(new Error(JSON.stringify(error as FirebaseError)));
  }
});

export const campaigns = functions.https.onRequest(app);
