import { firebaseApp } from './firebase-app';

const firestore = firebaseApp.firestore();
firestore.settings({ ignoreUndefinedProperties: true });

export { firestore };
