import firebase from 'firebase/app';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { AuthService } from '@app/modules/auth/services/auth/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'gm-helper';
  isLoading: boolean;

  constructor(private router: Router, private authService: AuthService) {}
  ngOnInit() {
    this.isLoading = true;
    firebase.auth().onAuthStateChanged((user) => {
      this.isLoading = false;
      if (user) {
        this.authService.user = user;
      } else {
        this.router.navigate(['/auth/login']);
      }
    });
  }
}
