export type NpcWithFile = Omit<Npc, 'src'> & {
  file?: File;
};

export type Npc = {
  id: number;
  name: string;
  settingId?: number;
  parameters?: any;
  description?: string;
  shortDescription?: string;
  src: string;
  userId: string;
};
