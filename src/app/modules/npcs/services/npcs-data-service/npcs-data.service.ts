import { Injectable } from '@angular/core';

import { NPCS_URL } from '@app/modules/npcs/utils/constants';
import { Npc } from '@app/modules/npcs/types/npc';
import { DataService, LocalStorageKeys } from '@app/services/data/data.service';

@Injectable({
  providedIn: 'root',
})
export class NpcsDataService extends DataService<Npc> {
  dataUrl = NPCS_URL;
  localStorageKey = LocalStorageKeys.Npcs;

  listNpcs = this.listEntities;
  getNpc = this.getEntity;
  createNpc = this.createEntity;
  updateNpc = this.updateEntity;
  deleteNpc = this.deleteEntity;
}
