import { TestBed } from '@angular/core/testing';

import { NpcsDataService } from './npcs-data.service';

describe('NpcsDataServiceService', () => {
  let service: NpcsDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {
          provide: NpcsDataService,
          useValue: {},
        },
      ],
    });
    service = TestBed.inject(NpcsDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
