import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

import { NpcWithFile } from '@app/modules/npcs/types/npc';
import { Npc as NpcConstructor } from '@app/modules/npcs/utils/npc';

@Component({
  selector: 'app-npc-creation-dialog',
  templateUrl: './npc-creation-dialog.component.html',
  styleUrls: ['./npc-creation-dialog.component.scss'],
})
export class NpcCreationDialogComponent implements OnInit {
  npcEditorOpened = false;
  npcSrc: SafeResourceUrl;
  file: File = null;

  constructor(
    public dialogRef: MatDialogRef<NpcCreationDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: NpcWithFile,
    private sanitizer: DomSanitizer
  ) {}

  ngOnInit() {
    this.data = new NpcConstructor(this.data);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  handleNpcImageInput(files: FileList) {
    const file = files[0];
    const objectUrl = URL.createObjectURL(file);
    // TODO: Ensure that this is safe
    const sanitisedObjectUrl = this.sanitizer.bypassSecurityTrustResourceUrl(
      objectUrl
    );

    this.npcSrc = sanitisedObjectUrl;
    this.file = file;

    this.data = {
      ...this.data,
      file,
    };
  }

  handleEditorEmit(description: string) {
    this.data = {
      ...this.data,
      description,
    };

    this.closeNpcEditor();
  }

  openNpcEditor() {
    this.npcEditorOpened = true;
  }

  closeNpcEditor() {
    this.npcEditorOpened = false;
  }
}
