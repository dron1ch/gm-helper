import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { Observable } from 'rxjs';

import { NpcComponent } from './npc.component';
import { NpcsDataService } from '../../services/npcs-data-service/npcs-data.service';
import { RouterModule } from '@angular/router';

describe('NpcComponent', () => {
  let component: NpcComponent;
  let fixture: ComponentFixture<NpcComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        providers: [
          {
            provide: NpcsDataService,
            useValue: {
              getNpc: () => new Observable(),
            },
          },
        ],
        imports: [
          RouterModule.forRoot([], { relativeLinkResolution: 'legacy' }),
        ],
        declarations: [NpcComponent],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(NpcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
