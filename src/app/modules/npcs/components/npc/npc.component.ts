import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { NpcsDataService } from '@app/modules/npcs/services/npcs-data-service/npcs-data.service';
import { Npc } from '@app/modules/npcs/types/npc';

@Component({
  selector: 'app-npc',
  templateUrl: './npc.component.html',
  styleUrls: ['./npc.component.scss'],
})
export class NpcComponent implements OnInit {
  npc: Npc = null;
  npcEditorOpened = false;
  isLoading = false;

  constructor(
    private route: ActivatedRoute,
    private npcsDataService: NpcsDataService
  ) {}

  ngOnInit(): void {
    const npcId = parseInt(this.route.snapshot.paramMap.get('npcId'), 10);
    this.npcsDataService.getNpc(npcId).subscribe((npc) => {
      this.npc = npc;
    });
  }

  openNpcEditor() {
    this.npcEditorOpened = true;
  }

  handleEditorEmit(description: string) {
    this.isLoading = true;
    this.saveNpc({
      ...this.npc,
      description,
    }).subscribe({
      next: (npc) => {
        this.isLoading = false;
        this.npcEditorOpened = false;
        this.npc = npc;
      },
    });
  }

  private saveNpc(npc: Npc) {
    return this.npcsDataService.updateNpc(npc.id, npc);
  }
}
