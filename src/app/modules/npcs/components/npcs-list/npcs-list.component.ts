import { Component, OnInit, Input } from '@angular/core';

import { MatDialog } from '@angular/material/dialog';

import { AuthService } from '@app/modules/auth/services/auth/auth.service';
import { Npc, NpcWithFile } from '@app/modules/npcs/types/npc';
import { NpcsDataService } from '@app/modules/npcs/services/npcs-data-service/npcs-data.service';
import { NpcCreationDialogComponent } from '@app/modules/npcs/components/npc-creation-dialog/npc-creation-dialog.component';
import { ImagesService } from '@app/services/images/images.service';
import { ErrorsService } from '@app/services/errors/errors.service';
import { validateNpc } from '@app/modules/npcs/utils/validation';

@Component({
  selector: 'app-npcs-list',
  templateUrl: './npcs-list.component.html',
  styleUrls: ['./npcs-list.component.scss'],
})
export class NpcsListComponent implements OnInit {
  @Input() settingId: number;

  isLoading: boolean;
  npcs: Npc[];

  constructor(
    private authService: AuthService,
    private npcsDataService: NpcsDataService,
    private imagesService: ImagesService,
    private errorsService: ErrorsService,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.isLoading = true;

    const user = this.authService.user;
    const filter = { settingId: this.settingId, userId: user.uid };

    this.npcsDataService
      .listNpcs({
        filter,
      })
      .subscribe((npcs) => {
        this.npcs = npcs instanceof Array ? npcs : [];
        this.isLoading = false;
      });
  }

  openNpcDialog(): void {
    const user = this.authService.user;

    const dialogRef = this.dialog.open(NpcCreationDialogComponent, {
      width: '150rem',
      data: { settingId: this.settingId, userId: user.uid } as {
        settingId: number;
        userId: string;
      },
    });

    dialogRef.afterClosed().subscribe((result: NpcWithFile) => {
      if (result) {
        const [isValid, validationError] = validateNpc(result);
        if (isValid) {
          this.addNpcToList({ ...result });
        } else {
          this.errorsService.emitError(validationError);
        }
      }
    });
  }

  addNpcToList(npcWithFile: NpcWithFile) {
    this.isLoading = true;

    try {
      this.imagesService
        .saveImage(`npcs/${this.authService.user.uid}`, npcWithFile.file)
        .subscribe(
          (src) => {
            const { file, ...npcWithoutFile } = npcWithFile;
            const settingNpc = {
              ...npcWithoutFile,
              src,
            };

            this.npcsDataService.createNpc(settingNpc).subscribe(
              (createdNpc) => {
                this.npcs.push(createdNpc);
                this.isLoading = false;
              },
              (error) => {
                this.errorsService.emitError(error);
                this.isLoading = false;
              }
            );
          },
          (error) => {
            this.errorsService.emitError(error);
            this.isLoading = false;
          }
        );
    } catch (error) {
      this.errorsService.emitError(error);
      this.isLoading = false;
    }
  }

  removeNpcFromList(npcId: number) {
    this.isLoading = true;

    this.npcsDataService.deleteNpc(npcId).subscribe(() => {
      this.npcs = this.npcs.filter((n) => n.id !== npcId);
      this.isLoading = false;
    });
  }
}
