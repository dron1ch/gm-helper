import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatDialogModule } from '@angular/material/dialog';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatListModule } from '@angular/material/list';

import { NpcCreationDialogComponent } from '@app/modules/npcs/components/npc-creation-dialog/npc-creation-dialog.component';
import { NpcComponent } from '@app/modules/npcs/components/npc/npc.component';
import { QuillModule } from '@app/modules/quill/quill.module';
import { NpcsListComponent } from '@app/modules/npcs/components/npcs-list/npcs-list.component';
@NgModule({
  declarations: [NpcCreationDialogComponent, NpcComponent, NpcsListComponent],
  imports: [
    FormsModule,
    CommonModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatDialogModule,
    MatIconModule,
    MatProgressSpinnerModule,
    MatListModule,
    RouterModule,
    QuillModule,
  ],
  exports: [NpcCreationDialogComponent, NpcsListComponent],
})
export class NpcsModule {}
