export const NPC_NAME_STUB = 'My Legendary NPC';
export const NPC_DESCRIPTION_STUB = 'His deeds were unmatched...';
export const NPC_SHORT_DESCRIPTION_STUB = 'Lazy Bard';
export const NPCS_URL =
  'https://us-central1-gm-helper-d65e8.cloudfunctions.net/npcs';
