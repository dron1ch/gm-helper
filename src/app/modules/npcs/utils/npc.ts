import {
  NPC_NAME_STUB,
  NPC_DESCRIPTION_STUB,
  NPC_SHORT_DESCRIPTION_STUB,
} from '@app/modules/npcs/utils/constants';
import { NpcWithFile as TNpc } from '@app/modules/npcs/types/npc';
import { generateId } from '@app/utils/generate-id/generate-id';

export class Npc implements TNpc {
  id = generateId();
  name = NPC_NAME_STUB;
  description = NPC_DESCRIPTION_STUB;
  shortDescription = NPC_SHORT_DESCRIPTION_STUB;
  parameters: any = {};
  campaignId?: number;
  settingId?: number;
  file?: File = null;
  userId: string;

  constructor(options?: Partial<TNpc> & { userId: string }) {
    const { name, parameters, settingId, file, userId } = options;

    this.name = name ?? this.name;
    this.parameters = parameters ?? this.parameters;
    this.settingId = settingId ?? this.settingId;
    this.file = file ?? this.file;
    this.userId = userId;
  }
}
