import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { NpcsDataService } from '@app/modules/npcs/services/npcs-data-service/npcs-data.service';
import { LocationsDataService } from '@app/modules/locations/services/locations-data/locations-data.service';
import { Npc } from '@app/modules/npcs/types/npc';
import { Location } from '@app/modules/locations/types/location';

export enum DataDialogDataType {
  NPC = 'npc',
  LOCATION = 'location',
}

export type NpcData = {
  id: number;
  type: DataDialogDataType.NPC;
};

export type LocationData = {
  id: number;
  type: DataDialogDataType.LOCATION;
};

export type DataDialogData = NpcData | LocationData;

@Component({
  selector: 'app-data-dialog',
  templateUrl: './data-dialog.component.html',
  styleUrls: ['./data-dialog.component.scss'],
})
export class DataDialogComponent implements OnInit {
  locationInfo: Partial<Location> = null;
  npcInfo: Partial<Npc> = null;
  isLoading = false;

  constructor(
    public dialogRef: MatDialogRef<DataDialogComponent>,
    private npcsDataService: NpcsDataService,
    private locationsDataService: LocationsDataService,
    @Inject(MAT_DIALOG_DATA) public data: DataDialogData
  ) {}

  ngOnInit(): void {
    if (this.data.type === DataDialogDataType.LOCATION) {
      this.isLoading = true;

      this.locationsDataService
        .getLocation(this.data.id)
        .subscribe((location) => {
          this.locationInfo = {
            ...location,
          };
          this.isLoading = false;
        });
    }
    if (this.data.type === DataDialogDataType.NPC) {
      this.isLoading = true;
      this.npcsDataService.getNpc(this.data.id).subscribe((npc) => {
        this.npcInfo = {
          ...npc,
        };
        this.isLoading = false;
      });
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
