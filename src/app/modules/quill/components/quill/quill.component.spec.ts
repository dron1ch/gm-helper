import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { Observable } from 'rxjs';

import { MatMenuModule } from '@angular/material/menu';

import { NpcsDataService } from '@app/modules/npcs/services/npcs-data-service/npcs-data.service';

import { QuillComponent } from './quill.component';

describe('QuillComponent', () => {
  let component: QuillComponent;
  let fixture: ComponentFixture<QuillComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      providers: [
        {
          provide: NpcsDataService,
          useValue: {
            listNpcs: () => new Observable(),
          },
        },
      ],
      imports: [MatMenuModule, HttpClientTestingModule, RouterTestingModule],
      declarations: [QuillComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuillComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
