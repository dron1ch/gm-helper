import {
  Component,
  ElementRef,
  AfterViewInit,
  Input,
  EventEmitter,
  Output,
  NgZone,
  ChangeDetectorRef,
  OnInit,
} from '@angular/core';
import Quill, { RangeStatic, StringMap } from 'quill';
import { Observable } from 'rxjs';

import { getQuillControlsConfigsBlot } from '@app/modules/quill/utils/configs';
import {
  QuillControlsBindProps,
  QuillAbstractionVariant,
} from '@app/modules/quill/utils/types';
import { NpcBlot, LocationBlot } from '@app/modules/quill/utils/blots';
import { NpcsDataService } from '@app/modules/npcs/services/npcs-data-service/npcs-data.service';
import { Npc } from '@app/modules/npcs/types/npc';
import { Location } from '@app/modules/locations/types/location';
import { AuthService } from '@app/modules/auth/services/auth/auth.service';
import { CampaignsDataService } from '@app/modules/campaigns/services/campaigns-data/campaigns-data.service';
import { ActivatedRoute } from '@angular/router';
import { LocationsDataService } from '@app/modules/locations/services/locations-data/locations-data.service';

type Format = 'bold' | 'italic' | 'underline' | 'blockquote' | 'list' | 'size';

type AdvancedFormat = 'npc' | 'location';

type ToolbarButtonsActiveState = {
  bold: boolean;
  italic: boolean;
  underline: boolean;
  blockquote: boolean;
  list: boolean;
  npc: boolean;
  location: boolean;
  size: boolean;
};

@Component({
  selector: 'app-quill',
  templateUrl: './quill.component.html',
  styleUrls: ['./quill.component.scss'],
})
export class QuillComponent implements OnInit, AfterViewInit {
  @Input() content = '';
  @Input() npcEditorActive = false;
  @Input() locationEditorActive = false;
  @Input() shouldSaveOnBlur = false;
  @Output() dataEmit = new EventEmitter();
  @Input() settingId: number;

  quill: Quill;
  toolbarButtonsActiveState: ToolbarButtonsActiveState = {
    bold: false,
    italic: false,
    underline: false,
    blockquote: false,
    list: false,
    npc: false,
    location: false,
    size: false,
  };
  npcs$: Observable<Npc[]>;
  locations$: Observable<Location[]>;

  constructor(
    private elRef: ElementRef,
    private ngZone: NgZone,
    private changeDetectorRef: ChangeDetectorRef,
    private npcsDataService: NpcsDataService,
    private campaignsDataService: CampaignsDataService,
    private locationsDataService: LocationsDataService,
    private authService: AuthService,
    private route: ActivatedRoute
  ) {}

  handleClickOnControl(targetFormat: Format | AdvancedFormat, data?: any) {
    const selectedText = this.getSelectedText();
    const formats = this.getFormat(selectedText);
    const selectedTextFormatted = formats[targetFormat];

    if (selectedTextFormatted) {
      this.removeFormat(selectedText);
      const updatedFormats = Object.keys(formats).reduce(
        (acc, format) =>
          format === targetFormat
            ? acc
            : {
                ...acc,
                [format]: formats[format],
              },
        {}
      );
      this.setFormats(updatedFormats);
    } else {
      this.format(targetFormat, data);
    }

    this.updateControlsState(selectedText);
  }

  ngAfterViewInit() {
    this.setEditorCursor('end');
  }

  ngOnInit() {
    this.setupGeneralControlsLogic();
    this.setupAttributorsLogic();
    this.registerCustomFormats();
    this.initialiseQuill();
    this.setupHandlers();

    if (this.npcEditorActive) {
      this.setupNpcEditor();
    }
    if (this.locationEditorActive) {
      this.setupLocationEditor();
    }
  }

  emitData() {
    const innerHtml = this.quill.root.innerHTML;
    this.dataEmit.emit(innerHtml);
  }

  private setupNpcEditor() {
    this.route.url.subscribe((url) => {
      const campaignId = parseInt(url[url.length - 1].path, 10);
      // TODO: stuff like this should be put in localStorage
      this.campaignsDataService
        .getCampaign(campaignId)
        .subscribe((campaign) => {
          // TODO: make common types between filters on frontend and backend
          const filter = {
            filter: {
              settingId: campaign.settingId,
              userId: this.authService.user.uid,
            },
          };

          this.npcs$ = this.npcsDataService.listNpcs(filter);
        });
    });
  }

  private setupLocationEditor() {
    this.route.url.subscribe((url) => {
      const campaignId = parseInt(url[url.length - 1].path, 10);
      // TODO: stuff like this should be put in localStorage
      this.campaignsDataService
        .getCampaign(campaignId)
        .subscribe((campaign) => {
          // TODO: make common types between filters on frontend and backend
          const filter = {
            filter: {
              settingId: campaign.settingId,
              userId: this.authService.user.uid,
            },
          };

          this.locations$ = this.locationsDataService.listLocations(filter);
        });
    });
  }

  private initialiseQuill() {
    const quillWrapperElem = this.elRef.nativeElement.querySelector(
      '[name="quill-content-wrapper"]'
    );
    if (quillWrapperElem) {
      this.quill = new Quill(quillWrapperElem);
      this.quill.clipboard.dangerouslyPasteHTML(this.content);
    }
  }

  private setupGeneralControlsLogic() {
    const InlineBlot = Quill.import('blots/inline');

    const controlsInitializationProps = getQuillControlsConfigsBlot().map(
      (config) => ({
        ...config,
        proto: InlineBlot,
      })
    );

    controlsInitializationProps.map((control) => {
      this.bindQuillControlLogicToSingleControl(control);
    });
  }

  private setupAttributorsLogic() {
    const fontAttributor = Quill.import('attributors/class/font');
    const fontSizeAttributor = Quill.import('attributors/style/size');

    fontAttributor.whitelist = ['roboto'];
    fontSizeAttributor.whitelist = ['18px', '16px'];

    Quill.register(fontAttributor, true);
    Quill.register(fontSizeAttributor, true);
  }

  private setupHandlers() {
    // This happens on blur
    this.quill.on('selection-change', (range) => {
      if (!range && this.shouldSaveOnBlur) {
        this.emitData();
      } else {
        this.updateControlsState(range);
        // We need to trigger this one, because we have changed html inside quill manually
        this.changeDetectorRef.detectChanges();
      }
    });
  }

  private registerCustomFormats() {
    Quill.register({ 'formats/npc': NpcBlot }, true);
    Quill.register({ 'formats/location': LocationBlot }, true);
  }

  private setEditorCursor(position: 'start' | 'end') {
    if (this.quill) {
      if (position === 'start') {
        this.quill.focus();
      } else {
        const endSymbolPosition = this.quill.getLength() - 1;
        this.quill.setSelection(endSymbolPosition, endSymbolPosition);
      }
    }
  }

  private updateControlsState(range: RangeStatic) {
    this.ngZone.run(() => {
      const formats = this.quill.getFormat(range);

      this.toolbarButtonsActiveState = Object.keys(
        this.toolbarButtonsActiveState
      ).reduce(
        (acc, item) => ({ ...acc, [item]: !!formats[item] || false }),
        {} as ToolbarButtonsActiveState
      );
    });
  }

  private bindQuillControlLogicToSingleControl(
    bindProps: QuillControlsBindProps
  ) {
    const { proto, blotName, tagName, abstraction } = bindProps;

    class QuillAbstraction extends proto {}

    if (abstraction === QuillAbstractionVariant.Blot) {
      QuillAbstraction.blotName = blotName;
      QuillAbstraction.tagName = tagName;
    }

    if (abstraction === QuillAbstractionVariant.Attributor) {
      console.warn('No handler yet implemented for parchment attributors!');
    }

    Quill.register(QuillAbstraction);
  }

  private getSelectedText(): RangeStatic {
    return this.quill.getSelection();
  }

  private getFormat(selectedText: RangeStatic) {
    return this.quill.getFormat(selectedText.index, selectedText.length);
  }

  private removeFormat(selectedText: RangeStatic) {
    this.quill.removeFormat(selectedText.index, selectedText.length);
  }

  private setFormats(formats: StringMap) {
    Object.keys(formats).forEach((formatName) => {
      this.format(formatName, formats[formatName]);
    });
  }

  private format(formatName: string, value: any = true) {
    console.log(formatName, value);
    this.quill.format(formatName, value);
  }
}
