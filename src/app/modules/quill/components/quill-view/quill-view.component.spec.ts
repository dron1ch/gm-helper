import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { MatDialogModule } from '@angular/material/dialog';

import { QuillViewComponent } from './quill-view.component';
import { SafeHtmlPipe } from '@src/app/modules/pipes/safe-html/safe-html.pipe';
import { PrettifyEmptyQuillOutputPipe } from '@app/modules/quill/pipes/prettify-empty-quill-output.pipe';

describe('QuillViewComponent', () => {
  let component: QuillViewComponent;
  let fixture: ComponentFixture<QuillViewComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [MatDialogModule],
      declarations: [
        QuillViewComponent,
        SafeHtmlPipe,
        PrettifyEmptyQuillOutputPipe,
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuillViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
