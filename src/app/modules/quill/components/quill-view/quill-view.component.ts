import { Component, ElementRef, AfterViewInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import {
  DataDialogComponent,
  DataDialogData,
  DataDialogDataType,
} from '@app/modules/quill/components/data-dialog/data-dialog.component';

@Component({
  selector: 'app-quill-view',
  templateUrl: './quill-view.component.html',
  styleUrls: ['./quill-view.component.scss'],
})
export class QuillViewComponent implements AfterViewInit {
  @Input() content: string;
  constructor(private elementRef: ElementRef, public dataDialog: MatDialog) {}

  ngAfterViewInit() {
    // Yep, no way around this. We get custom html from quill, so we have to deal with this raw DOM here.
    const npcNativeLinks = this.elementRef.nativeElement.getElementsByClassName(
      'npc-link'
    ) as HTMLElement[];

    const locationNativeLinks = this.elementRef.nativeElement.getElementsByClassName(
      'location-link'
    ) as HTMLElement[];

    this.registerNpcLinksLogic(npcNativeLinks);
    this.registerLocationLinksLogic(locationNativeLinks);
  }

  private registerNpcLinksLogic(npcNativeLinks: HTMLElement[]) {
    for (const npcNativeLink of npcNativeLinks) {
      this.registerNpcDialogLogic(npcNativeLink);
      this.registerHoverLogic(npcNativeLink);
    }
  }

  private registerNpcDialogLogic(npcNativeLink: HTMLElement) {
    npcNativeLink.addEventListener('click', (event) => {
      const target = event.currentTarget as HTMLElement;
      this.openDataDialog({
        id: +target.dataset.npcId,
        type: DataDialogDataType.NPC,
      });
    });
  }

  private registerLocationLinksLogic(locationNativeLinks: HTMLElement[]) {
    for (const locationNativeLink of locationNativeLinks) {
      this.registerLocationDialogLogic(locationNativeLink);
      this.registerHoverLogic(locationNativeLink);
    }
  }

  private registerLocationDialogLogic(locationNativeLink: HTMLElement) {
    locationNativeLink.addEventListener('click', (event) => {
      const target = event.currentTarget as HTMLElement;
      this.openDataDialog({
        id: +target.dataset.locationId,
        type: DataDialogDataType.LOCATION,
      });
    });
  }

  private registerHoverLogic(nativeLink: HTMLElement) {
    nativeLink.addEventListener('mouseenter', (event) => {
      const target = event.currentTarget as HTMLElement;
      target.style.cursor = 'pointer';
    });

    nativeLink.addEventListener('mouseleave', (event) => {
      const target = event.currentTarget as HTMLElement;
      target.style.cursor = 'default';
    });
  }

  private openDataDialog(data: DataDialogData) {
    this.dataDialog.open(DataDialogComponent, {
      data,
      maxHeight: '75vh',
    });
  }
}
