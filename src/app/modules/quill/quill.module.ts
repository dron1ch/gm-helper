import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatMenuModule } from '@angular/material/menu';
import { MatDialogModule } from '@angular/material/dialog';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

import { QuillComponent } from '@app/modules/quill/components/quill/quill.component';
import { DataDialogComponent } from '@app/modules/quill/components/data-dialog/data-dialog.component';
import { QuillViewComponent } from '@app/modules/quill/components/quill-view/quill-view.component';
import { PipesModule } from '@app/modules/pipes/pipes.module';
import { PrettifyEmptyQuillOutputPipe } from './pipes/prettify-empty-quill-output.pipe';

@NgModule({
  declarations: [
    QuillComponent,
    DataDialogComponent,
    QuillViewComponent,
    PrettifyEmptyQuillOutputPipe,
  ],
  imports: [
    CommonModule,
    MatButtonModule,
    MatIconModule,
    MatToolbarModule,
    MatTooltipModule,
    MatMenuModule,
    MatDialogModule,
    MatProgressSpinnerModule,
    BrowserAnimationsModule,
    PipesModule,
  ],
  exports: [QuillComponent, QuillViewComponent],
})
export class QuillModule {}
