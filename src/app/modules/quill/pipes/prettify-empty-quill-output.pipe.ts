import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'prettifyEmptyQuillOutput',
})
export class PrettifyEmptyQuillOutputPipe implements PipeTransform {
  transform(unsafeHtml: string): string {
    const stringToCheckFor = '<p><br></p>';
    return unsafeHtml === stringToCheckFor ? '' : unsafeHtml;
  }
}
