import { QuillControlsBindProps } from '@app/modules/quill/utils/types';

// I have left it as a reminder how to quickly setup custom logic for controls
// const npcControlConfig: QuillControlsBindProps = {
//   proto: null,
//   blotName: 'npc',
//   tagName: 'section',
//   abstraction: QuillAbstractionVariant.BLOT,
// };
export const getQuillControlsConfigsBlot = (): QuillControlsBindProps[] => [];
