export enum QuillAbstractionVariant {
  Blot = 'blot',
  Attributor = 'attributor',
}

export type QuillControlsBindProps = {
  proto: any;
  blotName: string;
  tagName: string;
  abstraction: QuillAbstractionVariant;
};
