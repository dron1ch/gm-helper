import Quill from 'quill';

export const npcDatasetKey = 'npcId';

// This one is not typed for two reasons:
// 1) Typings are buggy (especially Node type)
// 2) If we want to use typings then have to import parent class from parchment
// package and for some reason I couldn't make it register via Quill.register call.

const Inline = Quill.import('blots/inline');

export class NpcBlot extends Inline {
  static blotName = 'npc';
  static tagName = 'span';
  static className = 'npc-link';

  constructor(domNode: HTMLElement) {
    super(domNode);
  }

  static create(value: any) {
    const node = super.create();
    node.dataset[npcDatasetKey] = value;
    return node;
  }

  static formats(domNode: HTMLElement) {
    return domNode.dataset[npcDatasetKey];
  }

  static value(domNode: HTMLElement) {
    return domNode.dataset[npcDatasetKey];
  }
}
