import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SafeHtmlPipe } from '@app/modules/pipes/safe-html/safe-html.pipe';
import { EnumToArrayPipe } from '@app/modules/pipes/enum-to-array/enum-to-array.pipe';
import { LoggerPipe } from './logger/logger.pipe';

@NgModule({
  declarations: [SafeHtmlPipe, EnumToArrayPipe, LoggerPipe],
  imports: [CommonModule],
  exports: [SafeHtmlPipe, EnumToArrayPipe, LoggerPipe],
})
export class PipesModule {}
