import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'logger',
})
export class LoggerPipe implements PipeTransform {
  transform(value: unknown): unknown {
    console.log(value);

    return value;
  }
}
