export type Campaign = {
  id: number;
  name: string;
  settingId?: number;
  chapters: Chapter[];
  userId: string;
};

export type Chapter = {
  name: string;
  content: string;
};
