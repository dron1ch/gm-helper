import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CHAPTER_NAME_STUB } from '@app/modules/campaigns/utils/constants';

export type ChapterCreationData = {
  name: string;
};

@Component({
  selector: 'app-chapter-creation-dialog',
  templateUrl: './chapter-creation-dialog.component.html',
  styleUrls: ['./chapter-creation-dialog.component.scss'],
})
export class ChapterCreationDialogComponent implements OnInit {
  placeholder = CHAPTER_NAME_STUB;
  constructor(
    public dialogRef: MatDialogRef<ChapterCreationDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ChapterCreationData
  ) {}

  ngOnInit() {
    if (!this.data.name) {
      this.data.name = this.placeholder;
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
