import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CAMPAIGN_NAME_STUB } from '@app/modules/campaigns/utils/constants';
import { Campaign } from '@app/modules/campaigns/utils/campaign';

@Component({
  selector: 'app-campaign-creation-dialog',
  templateUrl: './campaign-creation-dialog.component.html',
})
export class CampaignCreationDialogComponent implements OnInit {
  placeholder = CAMPAIGN_NAME_STUB;
  constructor(
    public dialogRef: MatDialogRef<CampaignCreationDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Campaign
  ) {}

  ngOnInit() {
    if (!this.data.name) {
      this.data = new Campaign(this.data);
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
