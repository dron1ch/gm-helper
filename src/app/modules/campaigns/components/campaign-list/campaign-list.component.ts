import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import { Campaign } from '@app/modules/campaigns/types/campaigns';
import { CampaignsDataService } from '@app/modules/campaigns/services/campaigns-data/campaigns-data.service';
import { CampaignCreationDialogComponent } from '@app/modules/campaigns/components/campaign-creation-dialog/campaign-creation-dialog.component';
import { AuthService } from '@app/modules/auth/services/auth/auth.service';

@Component({
  selector: 'app-campaign-list',
  templateUrl: './campaign-list.component.html',
  styleUrls: ['./campaign-list.component.scss'],
})
export class CampaignListComponent implements OnInit {
  @Input() settingId: number;

  campaigns: Campaign[];
  isLoading: boolean;

  constructor(
    private campaignsDataService: CampaignsDataService,
    public dialog: MatDialog,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    this.isLoading = true;
    const user = this.authService.user;
    const filter = { settingId: this.settingId, userId: user.uid };

    this.campaignsDataService
      .listCampaigns({
        filter,
      })
      .subscribe((campaigns) => {
        this.campaigns = campaigns instanceof Array ? campaigns : [];
        this.isLoading = false;
      });
  }

  openCampaignDialog() {
    const user = this.authService.user;

    const dialogRef = this.dialog.open(CampaignCreationDialogComponent, {
      width: '150rem',
      data: { settingId: this.settingId, userId: user.uid } as {
        settingId: number;
        userId: string;
      },
    });

    dialogRef.afterClosed().subscribe((result: Campaign) => {
      if (result) {
        this.addCampaignToList({ ...result });
      }
    });
  }

  addCampaignToList(campaign: Campaign) {
    this.isLoading = true;

    this.campaignsDataService
      .createCampaign(campaign)
      .subscribe((savedCampaign) => {
        this.campaigns = [...this.campaigns, savedCampaign];
        this.isLoading = false;
      });
  }

  removeCampaignFromList(campaignId: number) {
    this.isLoading = true;
    this.campaignsDataService.deleteCampaign(campaignId).subscribe(() => {
      this.campaigns = this.campaigns.filter((c) => c.id !== campaignId);
      this.isLoading = false;
    });
  }
}
