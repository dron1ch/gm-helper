import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ChapterComponent } from './chapter.component';
import { Chapter } from '../../utils/chapter';
import { SafeHtmlPipe } from '@app/modules/pipes/safe-html/safe-html.pipe';

describe('ChapterComponent', () => {
  let component: ChapterComponent;
  let fixture: ComponentFixture<ChapterComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      providers: [
        {
          provide: Chapter,
          useValue: {
            name: 'stub',
            content: '',
          },
        },
      ],
      declarations: [ChapterComponent, SafeHtmlPipe],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChapterComponent);
    component = fixture.componentInstance;
    component.chapter = {
      name: 'stub',
      content: '',
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
