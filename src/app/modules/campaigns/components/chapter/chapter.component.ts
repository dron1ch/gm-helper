import { Component, Input, NgZone, Output, EventEmitter } from '@angular/core';

import { Chapter } from '@app/modules/campaigns/types/campaigns';

@Component({
  selector: 'app-chapter',
  templateUrl: './chapter.component.html',
  styleUrls: ['./chapter.component.scss'],
})
export class ChapterComponent {
  @Input() chapter: Chapter;
  @Output() updateChapter = new EventEmitter<Chapter>();

  isEditorOpened = false;
  chapterEditorOpened = false;

  constructor(private ngZone: NgZone) {}

  handleEditorEmit(data: string) {
    this.chapter = {
      ...this.chapter,
      content: data,
    };

    this.updateChapter.emit(this.chapter);
    this.closeChapterEditor();
  }

  openChapterEditor() {
    this.chapterEditorOpened = true;
  }

  closeChapterEditor() {
    this.ngZone.run(() => {
      this.chapterEditorOpened = false;
    });
  }
}
