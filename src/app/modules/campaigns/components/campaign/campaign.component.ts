import {
  Component,
  OnInit,
  ViewChildren,
  QueryList,
  NgZone,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';

import { Campaign, Chapter } from '@app/modules/campaigns/types/campaigns';
import { CampaignsDataService } from '@app/modules/campaigns/services/campaigns-data/campaigns-data.service';
import {
  ChapterCreationDialogComponent,
  ChapterCreationData,
} from '@app/modules/campaigns/components/chapter-creation-dialog/chapter-creation-dialog.component';
import { Chapter as ChapterConstructor } from '@app/modules/campaigns/utils/chapter';
import { ChapterComponent } from '@app/modules/campaigns/components/chapter/chapter.component';
import { ErrorsService } from '@src/app/services/errors/errors.service';

@Component({
  selector: 'app-campaign',
  templateUrl: './campaign.component.html',
  styleUrls: ['./campaign.component.scss'],
})
export class CampaignComponent implements OnInit {
  @ViewChildren(ChapterComponent) viewChildren: QueryList<ChapterComponent>;
  campaign: Campaign;
  isDataLoading: boolean;
  newChapterIsLoading: boolean;
  chapterIndexIsLoading: number = null;

  constructor(
    private route: ActivatedRoute,
    private campaignsDataService: CampaignsDataService,
    public dialog: MatDialog,
    private errorsService: ErrorsService,
    private ngZone: NgZone
  ) {}

  ngOnInit(): void {
    this.isDataLoading = true;
    this.newChapterIsLoading = false;
    const campaignId = parseInt(
      this.route.snapshot.paramMap.get('campaignId'),
      10
    );
    this.campaignsDataService.getCampaign(campaignId).subscribe(
      (updatedCampaign) => this.updateCampaign(updatedCampaign),
      (error) => this.handleError(error)
    );
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(ChapterCreationDialogComponent, {
      width: '150rem',
      data: {},
    });

    dialogRef.afterClosed().subscribe((result: ChapterCreationData) => {
      if (result) {
        const { name } = result;
        const newChapter = new ChapterConstructor({
          name,
          content: '',
        });
        this.addNewChapter(newChapter);
      }
    });
  }

  addNewChapter(chapter: Chapter) {
    const chapters = [...this.campaign.chapters, chapter];
    this.newChapterIsLoading = true;

    this.campaignsDataService
      .updateCampaign(this.campaign.id, { ...this.campaign, chapters })
      .subscribe(
        (updatedCampaign) => this.addChapter(updatedCampaign),
        (error) => this.handleError(error)
      );
  }

  removeChapter(chapterIndex: number) {
    this.chapterIndexIsLoading = chapterIndex;

    const chapters = [...this.campaign.chapters];
    chapters.splice(chapterIndex, 1);

    this.campaignsDataService
      .updateCampaign(this.campaign.id, { ...this.campaign, chapters })
      .subscribe(
        (updatedCampaign) => this.updateChapter(updatedCampaign),
        (error) => this.handleError(error)
      );
  }

  handleUpdateChapter(chapter: Chapter, chapterIndex: number) {
    const chapters = [...this.campaign.chapters];
    chapters[chapterIndex] = chapter;

    this.chapterIndexIsLoading = chapterIndex;

    this.campaignsDataService
      .updateCampaign(this.campaign.id, { ...this.campaign, chapters })
      .subscribe(
        (updatedCampaign) => this.updateChapter(updatedCampaign),
        (error) => this.handleError(error)
      );
  }

  openChapterEditor(chapterIndex: number) {
    const targetEditor = this.viewChildren.filter(
      (_, index) => index === chapterIndex
    );

    targetEditor[0].openChapterEditor();
  }

  private updateCampaign(updatedCampaign: Campaign) {
    this.ngZone.run(() => {
      this.campaign = updatedCampaign;
      this.isDataLoading = false;
    });
  }

  private addChapter(updatedCampaign: Campaign) {
    this.ngZone.run(() => {
      this.campaign = updatedCampaign;
      this.newChapterIsLoading = false;
    });
  }

  private updateChapter(updatedCampaign: Campaign) {
    this.ngZone.run(() => {
      this.campaign = updatedCampaign;
      this.chapterIndexIsLoading = null;
    });
  }

  private handleError(error: any) {
    this.errorsService.emitError(error);
    this.isDataLoading = false;
  }
}
