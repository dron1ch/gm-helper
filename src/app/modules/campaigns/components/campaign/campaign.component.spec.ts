import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { RouterModule } from '@angular/router';

import { CampaignComponent } from './campaign.component';
import { CampaignsDataService } from '../../services/campaigns-data/campaigns-data.service';
import { MatDialogModule } from '@angular/material/dialog';
import { Observable } from 'rxjs';

describe('CampaignComponent', () => {
  let component: CampaignComponent;
  let fixture: ComponentFixture<CampaignComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        providers: [
          {
            provide: CampaignsDataService,
            useValue: {
              getCampaign: () => new Observable(),
            },
          },
        ],
        imports: [
          RouterModule.forRoot([], { relativeLinkResolution: 'legacy' }),
          MatDialogModule,
        ],
        declarations: [CampaignComponent],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
