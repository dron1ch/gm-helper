import { Chapter as TChapter } from '@app/modules/campaigns/types/campaigns';

import { CHAPTER_NAME_STUB } from '@app/modules/campaigns/utils/constants';

export class Chapter implements TChapter {
  name = CHAPTER_NAME_STUB;
  content = '';

  constructor(options: TChapter) {
    if (options) {
      const { name, content } = options;

      this.name = name;
      this.content = content;
    }
  }
}
