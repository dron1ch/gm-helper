import {
  Campaign as TCampaign,
  Chapter,
} from '@app/modules/campaigns/types/campaigns';

import { CAMPAIGN_NAME_STUB } from '@app/modules/campaigns/utils/constants';
import { generateId } from '@app/utils/generate-id/generate-id';

export class Campaign implements TCampaign {
  id: number = generateId();
  name: string = CAMPAIGN_NAME_STUB;
  settingId: number;
  chapters: Chapter[] = [];
  userId: string;

  constructor(options?: Partial<TCampaign> & { userId: string }) {
    if (options) {
      const { name, chapters, settingId, userId } = options;

      if (settingId) {
        this.settingId = settingId;
      }

      this.name = name ?? this.name;
      this.chapters = chapters ?? this.chapters;
      this.userId = userId;
    }
  }
}
