import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatFormFieldModule } from '@angular/material/form-field';

import { AppRoutingModule } from '@app/app-routing.module';
import { ChapterComponent } from '@app/modules/campaigns/components/chapter/chapter.component';
import { CampaignComponent } from '@app/modules/campaigns/components/campaign/campaign.component';
import { CampaignsDataService } from '@app/modules/campaigns/services/campaigns-data/campaigns-data.service';
import { CampaignListComponent } from '@app/modules/campaigns/components/campaign-list/campaign-list.component';
import { CampaignCreationDialogComponent } from '@app/modules/campaigns/components/campaign-creation-dialog/campaign-creation-dialog.component';
import { QuillModule } from '@app/modules/quill/quill.module';
import { PipesModule } from '@app/modules/pipes/pipes.module';
import { ChapterCreationDialogComponent } from '@app/modules/campaigns/components/chapter-creation-dialog/chapter-creation-dialog.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

@NgModule({
  declarations: [
    ChapterComponent,
    CampaignComponent,
    CampaignListComponent,
    CampaignCreationDialogComponent,
    ChapterCreationDialogComponent,
  ],
  imports: [
    QuillModule,
    FormsModule,
    CommonModule,
    MatListModule,
    MatIconModule,
    MatInputModule,
    MatDialogModule,
    MatButtonModule,
    MatTooltipModule,
    AppRoutingModule,
    MatFormFieldModule,
    MatProgressSpinnerModule,
    PipesModule,
  ],
  providers: [CampaignsDataService],
  exports: [CampaignListComponent],
})
export class CampaignsModule {}
