import { Injectable } from '@angular/core';

import { Campaign } from '@app/modules/campaigns/types/campaigns';
import { CAMPAIGNS_URL } from '@app/modules/campaigns/utils/constants';

import { DataService, LocalStorageKeys } from '@app/services/data/data.service';

@Injectable({
  providedIn: 'root',
})
export class CampaignsDataService extends DataService<Campaign> {
  dataUrl = CAMPAIGNS_URL;
  localStorageKey = LocalStorageKeys.Campaigns;
  listCampaigns = this.listEntities;
  getCampaign = this.getEntity;
  createCampaign = this.createEntity;
  updateCampaign = this.updateEntity;
  deleteCampaign = this.deleteEntity;
}
