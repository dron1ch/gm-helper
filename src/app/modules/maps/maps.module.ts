import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatToolbarModule } from '@angular/material/toolbar';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatMenuModule } from '@angular/material/menu';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatDividerModule } from '@angular/material/divider';
import { MatListModule } from '@angular/material/list';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

import { QuillModule } from '@app/modules/quill/quill.module';
import { PipesModule } from '@app/modules/pipes/pipes.module';
import { LocationsModule } from '@app/modules/locations/locations.module';

import { MapComponent } from '@app/modules/maps/components/map/map.component';
import { MapCreationDialogComponent } from '@app/modules/maps/components/map-creation-dialog/map-creation-dialog.component';
import { MapDisplayComponent } from '@app/modules/maps/components/map-display/map-display.component';
import { MapsListComponent } from '@app/modules/maps/components/maps-list/maps-list.component';

@NgModule({
  declarations: [
    MapComponent,
    MapCreationDialogComponent,
    MapDisplayComponent,
    MapsListComponent,
  ],
  imports: [
    CommonModule,
    MatCardModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    MatTooltipModule,
    MatMenuModule,
    MatFormFieldModule,
    MatDialogModule,
    MatInputModule,
    MatDividerModule,
    MatListModule,
    MatProgressSpinnerModule,
    FormsModule,
    QuillModule,
    LocationsModule,
    PipesModule,
    RouterModule,
  ],
  exports: [MapComponent, MapsListComponent],
})
export class MapsModule {}
