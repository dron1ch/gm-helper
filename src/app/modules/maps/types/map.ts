import { Location } from '@app/modules/locations/types/location';

export type MapWithFile = Omit<Map, 'src'> & {
  file: File;
};

export type Map = {
  id: number;
  settingId?: number;
  name: string;
  src: string;
  markers: MapMarker[];
  userId: string;
};

export type MapMarker = {
  initialPosition: [number, number];
  style: {
    top: string;
    left: string;
    width: string;
    height: string;
    fontSize: string;
  };
  locationData: Location;
};
