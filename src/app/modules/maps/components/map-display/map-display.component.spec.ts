import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { RouterModule } from '@angular/router';
import { Observable } from 'rxjs';

import { MapDisplayComponent } from './map-display.component';
import { MapsDataService } from '@app/modules/maps/services/maps-data-service/maps-data.service';

describe('MapDisplayComponent', () => {
  let component: MapDisplayComponent;
  let fixture: ComponentFixture<MapDisplayComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        providers: [
          {
            provide: MapsDataService,
            useValue: { getMap: () => new Observable() },
          },
        ],
        imports: [
          RouterModule.forRoot([], { relativeLinkResolution: 'legacy' }),
        ],
        declarations: [MapDisplayComponent],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(MapDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
