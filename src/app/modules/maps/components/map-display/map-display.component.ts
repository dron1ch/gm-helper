import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { MapsDataService } from '@app/modules/maps/services/maps-data-service/maps-data.service';
import { Map, MapWithFile } from '@app/modules/maps/types/map';

@Component({
  selector: 'app-map-display',
  templateUrl: './map-display.component.html',
  styleUrls: ['./map-display.component.scss'],
})
export class MapDisplayComponent implements OnInit {
  map: MapWithFile & Map;
  constructor(
    private route: ActivatedRoute,
    private mapsDataService: MapsDataService
  ) {}

  ngOnInit(): void {
    const mapId = parseInt(this.route.snapshot.paramMap.get('mapId'), 10);
    this.mapsDataService.getMap(mapId).subscribe((map) => {
      this.map = map as MapWithFile & Map;
    });
  }
}
