import { zip, forkJoin, of } from 'rxjs';
import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import { Map, MapWithFile } from '@app/modules/maps/types/map';
import { MapsDataService } from '@app/modules/maps/services/maps-data-service/maps-data.service';
import { AuthService } from '@app/modules/auth/services/auth/auth.service';
import { MapCreationDialogComponent } from '@app/modules/maps/components/map-creation-dialog/map-creation-dialog.component';
import { ImagesService } from '@app/services/images/images.service';
import { LocationsDataService } from '@app/modules/locations/services/locations-data/locations-data.service';
import { ErrorsService } from '@app/services/errors/errors.service';
import { validateMap } from '@app/modules/maps/utils/validation';

@Component({
  selector: 'app-maps-list',
  templateUrl: './maps-list.component.html',
  styleUrls: ['./maps-list.component.scss'],
})
export class MapsListComponent implements OnInit {
  @Input() settingId: number;

  isLoading: boolean;
  maps: Map[];

  constructor(
    private mapsDataService: MapsDataService,
    private authService: AuthService,
    private imagesService: ImagesService,
    private locationsDataService: LocationsDataService,
    private errorsService: ErrorsService,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.isLoading = true;

    const user = this.authService.user;
    const filter = { settingId: this.settingId, userId: user.uid };

    this.mapsDataService
      .listMaps({
        filter,
      })
      .subscribe((maps) => {
        this.maps = maps instanceof Array ? maps : [];
        this.isLoading = false;
      });
  }

  openMapDialog() {
    const user = this.authService.user;

    const dialogRef = this.dialog.open(MapCreationDialogComponent, {
      width: '150rem',
      data: { settingId: this.settingId, userId: user.uid } as {
        settingId: number;
        userId: string;
      },
    });

    dialogRef.afterClosed().subscribe((result: MapWithFile) => {
      if (result) {
        const [isValid, error] = validateMap(result);
        if (isValid) {
          this.addMapToList({ ...result });
        } else {
          this.errorsService.emitError(error);
        }
      }
    });
  }

  addMapToList(mapWithFile: MapWithFile) {
    this.isLoading = true;

    try {
      this.imagesService
        .saveImage(`maps/${this.authService.user.uid}`, mapWithFile.file)
        .subscribe(
          (src) => {
            const { file, ...mapWithoutFile } = mapWithFile;

            const settingMap = {
              ...mapWithoutFile,
              src,
            };

            zip(
              this.mapsDataService.createMap(settingMap),
              // TODO: we need to save stuff like this in batches
              // TODO: we need to subscribe our data sources to Subjects, so we won't need to somehow tell them that the state changed
              forkJoin(
                settingMap.markers.length
                  ? settingMap.markers.map((marker) =>
                      this.locationsDataService.createLocation({
                        ...marker.locationData,
                        settingId: this.settingId,
                      })
                    )
                  : of([])
              )
            ).subscribe(
              ([createdMap]) => {
                this.maps.push(createdMap);
                this.isLoading = false;
              },
              (error) => {
                this.errorsService.emitError(error);
                this.isLoading = false;
              }
            );
          },
          (error) => {
            this.errorsService.emitError(error);
            this.isLoading = false;
          }
        );
    } catch (error) {
      this.errorsService.emitError(error);
      this.isLoading = false;
    }
  }

  removeMapFromList(mapId: number) {
    this.isLoading = true;

    this.mapsDataService.deleteMap(mapId).subscribe(() => {
      this.maps = this.maps.filter((m) => m.id !== mapId);
      this.isLoading = false;
    });
  }
}
