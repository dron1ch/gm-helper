import {
  Component,
  ViewChild,
  Output,
  EventEmitter,
  Input,
  OnInit,
  AfterViewInit,
} from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { MatDialog } from '@angular/material/dialog';

import { MapMarker, MapWithFile, Map } from '@app/modules/maps/types/map';
import { LocationType, Location } from '@app/modules/locations/types/location';
import { LocationCreationDialogComponent } from '@app/modules/locations/components/location-creation-dialog/location-creation-dialog.component';
import { AuthService } from '@app/modules/auth/services/auth/auth.service';

enum EventType {
  MarkerPlacement = 'markerPlacement',
  MarkerRemoval = 'markerRemoval',
  ZoomIn = 'zoomIn',
  ZoomOut = 'zoomOut',
}

export type MapPictureData = Pick<MapWithFile, 'file' | 'markers'>;

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss'],
})
export class MapComponent implements OnInit, AfterViewInit {
  @Input() interactive = true;
  @Input() inputMap: MapWithFile & Map;
  @Output() mapEvent = new EventEmitter<MapPictureData>();

  @ViewChild('map') map: { nativeElement: HTMLDivElement };
  @ViewChild('img') img: { nativeElement: HTMLImageElement };

  mapMarkers: MapMarker[] = [];
  mapSrc: SafeResourceUrl = null;
  zoomFactor = 1;
  initialImageWidth: number;
  selectedMarker: LocationType = LocationType.Default;
  matTooltipDisabled = false;

  readonly markerTypes = LocationType;
  readonly eventTypes = EventType;
  readonly zoomStep: number = 0.1;
  readonly markerDimensions = 30;

  private mapFile: File;

  constructor(
    private sanitizer: DomSanitizer,
    private dialog: MatDialog,
    private authService: AuthService
  ) {}

  ngOnInit() {
    if (this.inputMap) {
      const { src, file, markers } = this.inputMap;
      if (src) {
        this.mapMarkers = markers;
        this.mapSrc = src;
      }
      if (file) {
        this.mapMarkers = markers;
        // this is not yet implemented
        this.mapSrc = file;
      }
    }
  }

  ngAfterViewInit() {
    if (this.mapSrc) {
      this.recalculateImageSize();
    }
  }

  handleImageInput(files: FileList) {
    const file = files[0];
    const objectUrl = URL.createObjectURL(file);
    // TODO: Ensure that this is safe
    const sanitisedObjectUrl = this.sanitizer.bypassSecurityTrustResourceUrl(
      objectUrl
    );

    this.mapSrc = sanitisedObjectUrl;
    this.mapFile = file;
    this.handleSave();
    this.recalculateImageSize();
  }

  handleClickOnMap(event: MouseEvent, eventType: EventType, payload?: any) {
    switch (eventType) {
      case EventType.MarkerPlacement:
        if (this.interactive) {
          return this.placeMarker(event);
        }
        break;
      case EventType.MarkerRemoval:
        return this.removeMarker(payload);
      case EventType.ZoomIn:
        return this.zoom('in');
      case EventType.ZoomOut:
        return this.zoom('out');
      default:
        return console.error('Unknown action type');
    }
  }

  selectMarker(locationType: LocationType) {
    this.selectedMarker = locationType;
  }

  private handleSave() {
    if (this.mapFile) {
      this.mapEvent.emit({
        file: this.mapFile,
        markers: this.mapMarkers,
      });
    }
  }

  private zoom(zoomType: 'in' | 'out') {
    this.zoomFactor =
      zoomType === 'in'
        ? this.zoomFactor + this.zoomStep
        : this.zoomFactor - this.zoomStep;

    this.hideMarkers();
    this.updateImageWidth();
    this.recalculateMarkerPositions();
    this.showMarkers();
  }

  private hideMarkers() {
    this.mapMarkers = this.mapMarkers.map((mapMarker) => ({
      ...mapMarker,
      style: {
        ...mapMarker.style,
        opacity: 0,
      },
    }));
  }

  private showMarkers() {
    this.mapMarkers = this.mapMarkers.map((mapMarker) => ({
      ...mapMarker,
      style: {
        ...mapMarker.style,
        opacity: 1,
      },
    }));
  }

  private recalculateImageSize(timeout: number = 0) {
    setTimeout(() => {
      this.initialImageWidth = this.map.nativeElement.clientWidth;
      this.img.nativeElement.style.width = this.initialImageWidth + 'px';
    }, timeout);
  }

  private recalculateMarkerPositions() {
    this.mapMarkers = this.mapMarkers.map((mapMarker) => ({
      ...mapMarker,
      style: {
        ...mapMarker.style,
        left: `${mapMarker.initialPosition[0] * this.zoomFactor}px`,
        top: `${mapMarker.initialPosition[1] * this.zoomFactor}px`,
      },
    }));
  }

  private openDialog(
    markerInfo: Omit<MapMarker, 'locationData'>,
    locationData: Pick<Location, 'locationType'>
  ): void {
    const user = this.authService.user;

    const dialogRef = this.dialog.open(LocationCreationDialogComponent, {
      width: '150rem',
      data: { ...locationData, userId: user.uid },
    });

    dialogRef.afterClosed().subscribe((locationDataFromForm: Location) => {
      if (locationDataFromForm) {
        this.mapMarkers.push({
          ...markerInfo,
          locationData: locationDataFromForm,
        });
        this.handleSave();
      }
    });
  }

  private placeMarker(event: MouseEvent) {
    const nativeImg = this.img.nativeElement;
    const rect = nativeImg.getBoundingClientRect();

    const x = event.clientX + nativeImg.scrollLeft - rect.left;
    const y = event.clientY + nativeImg.scrollTop - rect.top;

    const markerInfo = {
      initialPosition: this.setupMarkerInitialPosition(x, y),
      style: this.setupMarkerInitialStyles(
        x * this.zoomFactor,
        y * this.zoomFactor
      ),
    };

    const locationData = {
      locationType: this.selectedMarker,
    };

    this.openDialog(markerInfo, locationData);
  }

  private removeMarker(marker: MapMarker) {
    this.mapMarkers = this.mapMarkers.filter(
      (mapMarker) => mapMarker !== marker
    );
  }

  private updateImageWidth() {
    this.img.nativeElement.style.width =
      this.initialImageWidth * this.zoomFactor + 'px';
  }

  private setupMarkerInitialPosition(x: number, y: number): [number, number] {
    return this.zoomFactor === 1
      ? [x - this.markerDimensions / 2, y - this.markerDimensions + 5]
      : [
          (x - this.markerDimensions / 2) / this.zoomFactor,
          (y - this.markerDimensions + 5) / this.zoomFactor,
        ];
  }

  private setupMarkerInitialStyles(x: number, y: number) {
    const [left, top] = this.setupMarkerInitialPosition(x, y).map(
      (item) => `${item}px`
    );
    return {
      top,
      left,
      width: `${this.markerDimensions}px`,
      height: `${this.markerDimensions}px`,
      fontSize: `${this.markerDimensions}px`,
    };
  }
}
