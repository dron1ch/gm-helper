import { Component, OnInit, Inject } from '@angular/core';

import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { MapWithFile } from '@app/modules/maps/types/map';
import { Map as MapConstructor } from '@app/modules/maps/utils/map';
import { MapPictureData } from '@app/modules/maps/components/map/map.component';

export type PartialMapData = Omit<
  MapWithFile,
  'id' | 'campaignId' | 'settingId'
>;

@Component({
  selector: 'app-map-creation-dialog',
  templateUrl: './map-creation-dialog.component.html',
  styleUrls: ['./map-creation-dialog.component.scss'],
})
export class MapCreationDialogComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<MapCreationDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: MapWithFile
  ) {}

  ngOnInit() {
    this.data = new MapConstructor(this.data);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  handleMapSave(mapData: MapPictureData) {
    this.data = { ...this.data, ...mapData };
  }
}
