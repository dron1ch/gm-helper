import { Injectable } from '@angular/core';

import { MAPS_URL } from '@app/modules/maps/utils/constants';
import { Map } from '@app/modules/maps/types/map';
import { DataService, LocalStorageKeys } from '@app/services/data/data.service';

@Injectable({
  providedIn: 'root',
})
export class MapsDataService extends DataService<Omit<Map, 'file'>> {
  dataUrl = MAPS_URL;
  localStorageKey = LocalStorageKeys.Maps;

  listMaps = this.listEntities;
  getMap = this.getEntity;
  createMap = this.createEntity;
  updateMap = this.updateEntity;
  deleteMap = this.deleteEntity;
}
