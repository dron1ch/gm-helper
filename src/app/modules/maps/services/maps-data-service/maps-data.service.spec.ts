import { TestBed } from '@angular/core/testing';

import { MapsDataService } from './maps-data.service';

describe('MapDataServiceService', () => {
  let service: MapsDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {
          provide: MapsDataService,
          useValue: {},
        },
      ],
    });
    service = TestBed.inject(MapsDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
