import { MapWithFile as TMap, MapMarker } from '@app/modules/maps/types/map';
import { MAP_NAME_STUB } from '@app/modules/maps/utils/constants';
import { generateId } from '@app/utils/generate-id/generate-id';

export class Map implements TMap {
  id = generateId();
  name = MAP_NAME_STUB;
  settingId: number;
  markers: MapMarker[] = [];
  file: File = null;
  userId: string;

  constructor(options?: Partial<TMap> & { userId: string }) {
    const { name, settingId, markers, file, userId } = options;

    this.settingId = settingId ?? this.settingId;
    this.markers = markers ?? this.markers;
    this.name = name ?? this.name;
    this.file = file ?? this.file;
    this.userId = userId;
  }
}
