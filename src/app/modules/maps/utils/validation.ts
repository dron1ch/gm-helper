import { MapWithFile } from '@app/modules/maps/types/map';
import { FirebaseError } from 'firebase';

export const validateMap = (
  map: MapWithFile
): [boolean, FirebaseError | null] => {
  const requiredFields = ['id', 'name', 'file', 'settingId', 'userId'];
  const isValid =
    Object.keys(map).filter((key) => requiredFields.includes(key) && map[key])
      .length === requiredFields.length;

  if (isValid) {
    return [isValid, null];
  } else {
    const error: FirebaseError = {
      code: 'validation/some-fields-are-empty',
      message: `We expect this entity to contain the following fields: 'name', 'file'`,
      name: null,
    };
    return [isValid, error];
  }
};
