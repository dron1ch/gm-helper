import { Howl } from 'howler';
import { Subject } from 'rxjs';

import { STARTING_VOLUME } from '@app/modules/audio-player/utils/constants';
import { Injectable } from '@angular/core';

type Blob = string;

export type AudioItem = {
  name: string;
  checked: boolean;
  howl: Howl;
};

@Injectable({
  providedIn: 'root',
})
export class AudioListService {
  trackList$ = new Subject<AudioItem[]>();
  private trackList: AudioItem[] = [];

  constructor() {}

  addTracksToList(audioBlobs: { name: string; blob: Blob }[]) {
    const newTracks: AudioItem[] = audioBlobs.map(({ name, blob }) => ({
      name,
      checked: false,
      howl: new Howl({
        src: blob,
        format: ['mp3'],
        volume: STARTING_VOLUME * 0.1,
      }),
    }));

    this.trackList = [...this.trackList, ...newTracks];
    this.trackList$.next(this.trackList);
  }

  removeTracksFromList(indices: number[]) {
    for (let i = indices.length - 1; i >= 0; i--) {
      this.trackList[i].howl.unload();
      this.trackList.splice(indices[i], 1);
    }

    this.trackList$.next(this.trackList);
  }

  getTrackList() {
    return this.trackList;
  }

  setTrackList(trackList: AudioItem[]) {
    this.trackList = trackList;
    this.trackList$.next(trackList);
  }

  startTrack(index: number) {
    const track = this.trackList[index];

    if (track.howl.playing) {
      track.howl.pause();
    }

    track.howl.play();
    this.trackList$.next(this.trackList);
  }

  pauseTrack(index: number) {
    this.trackList[index].howl.pause();
    this.trackList$.next(this.trackList);
  }

  changeVolumeOnTrack(index: number, volume: number) {
    this.trackList[index].howl.volume(volume);
  }

  removeTrackFromList(index: number) {
    this.trackList[index].howl.unload();
    this.trackList.splice(index, 1);
    this.trackList$.next(this.trackList);
  }

  setRepeatForTrack(index: number, needToRepeat: boolean) {
    this.trackList[index].howl.loop(needToRepeat);
  }

  updateTrack(index: number, newTrack: AudioItem) {
    this.trackList[index] = newTrack;
    this.trackList$.next(this.trackList);
  }

  startCheckedTracks() {
    this.trackList.forEach((track, index) => {
      if (track.checked) {
        this.startTrack(index);
      }
    });
    this.trackList$.next(this.trackList);
  }

  pauseCheckedTracks() {
    this.trackList.forEach((track, index) => {
      if (track.checked) {
        this.pauseTrack(index);
      }
    });
    this.trackList$.next(this.trackList);
  }

  changeVolumeOnCheckedTracks(volume: number) {
    this.trackList.forEach((track) => {
      if (track.checked) {
        track.howl.volume(volume);
      }
    });
  }
}
