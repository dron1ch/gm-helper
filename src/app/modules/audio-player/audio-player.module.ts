import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatSliderModule } from '@angular/material/slider';
import { MatMenuModule } from '@angular/material/menu';
import { MatDividerModule } from '@angular/material/divider';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatCheckboxModule } from '@angular/material/checkbox';

import { PlayerComponent } from '@app/modules/audio-player/components/player/player.component';
import { AudioTrackListComponent } from '@app/modules/audio-player/components/audio-track-list/audio-track-list.component';
import { AudioPlayerComponent } from '@app/modules/audio-player/components/audio-player/audio-player.component';
import { AudioListService } from '@app/modules/audio-player/services/audio-list.service';
import { GroupPlayerComponent } from './components/group-player/group-player.component';

@NgModule({
  declarations: [
    PlayerComponent,
    AudioTrackListComponent,
    AudioPlayerComponent,
    GroupPlayerComponent,
  ],
  imports: [
    CommonModule,
    MatButtonModule,
    MatIconModule,
    MatSliderModule,
    MatMenuModule,
    MatDividerModule,
    MatTooltipModule,
    MatCheckboxModule,
  ],
  providers: [AudioListService],
  exports: [AudioPlayerComponent],
})
export class AudioPlayerModule {}
