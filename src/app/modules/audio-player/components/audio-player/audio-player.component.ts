import { Component, OnInit } from '@angular/core';

import {
  AudioListService,
  AudioItem,
} from '@app/modules/audio-player/services/audio-list.service';

interface HTMLInputEvent extends Event {
  target: HTMLInputElement & EventTarget;
}

@Component({
  selector: 'app-audio-player',
  templateUrl: './audio-player.component.html',
  styleUrls: ['./audio-player.component.scss'],
})
export class AudioPlayerComponent implements OnInit {
  trackList: AudioItem[];
  constructor(private audioListService: AudioListService) {}

  ngOnInit(): void {
    this.audioListService.trackList$.subscribe((audioItems) => {
      this.trackList = audioItems.filter((item) => item.howl.playing());
    });
  }

  addTracksToTrackList() {
    document.getElementById('sound-input').click();
  }

  handleUploadedAudio(event: HTMLInputEvent) {
    // We need to cast this one
    const audioFilesTarget = event.target;

    // Files property is a data agnostic Object with indexes in place of keys
    const audioFiles = Array.from(audioFilesTarget.files);

    if (audioFiles?.length) {
      const audioBlobs = audioFiles.map((audioFile) => ({
        name: audioFile.name,
        blob: URL.createObjectURL(audioFile),
      }));

      this.audioListService.addTracksToList(audioBlobs);
    }
  }
}
