import { Component, OnInit } from '@angular/core';
import {
  AudioListService,
  AudioItem,
} from '@app/modules/audio-player/services/audio-list.service';

@Component({
  selector: 'app-audio-track-list',
  templateUrl: './audio-track-list.component.html',
  styleUrls: ['./audio-track-list.component.scss'],
})
export class AudioTrackListComponent implements OnInit {
  trackList: AudioItem[] = [];
  allTracksChecked: boolean;
  constructor(private audioListService: AudioListService) {}

  ngOnInit(): void {
    this.audioListService.trackList$.subscribe((newTracks) => {
      this.trackList = newTracks;
      this.allTracksChecked = this.checkIfAllTracksChecked();
    });
  }

  stopPropagation(event: Event) {
    event.stopPropagation();
  }

  handleCheck(index: number) {
    this.trackList = this.trackList.map((track, i) => ({
      ...track,
      checked: i === index ? !track.checked : track.checked,
    }));

    this.audioListService.setTrackList(this.trackList);
  }

  checkIfAllTracksChecked(): boolean {
    return this.trackList.every((track) => track.checked);
  }

  checkIfSomeTracksChecked(): boolean {
    return (
      this.trackList.some((track) => track.checked) && !this.allTracksChecked
    );
  }

  checkUncheckAllTracks(checkAll: boolean) {
    const newTrackList = this.trackList.map((track) => ({
      ...track,
      checked: checkAll,
    }));

    this.audioListService.setTrackList(newTrackList);
  }

  deleteCheckedTracks() {
    const checkedIndices = [];
    this.trackList.forEach((track, index) => {
      if (track.checked) {
        checkedIndices.push(index);
      }
    });

    this.audioListService.removeTracksFromList(checkedIndices);
  }

  cutTrackName(trackName: string) {
    const limit = 45;
    const threeDotsLength = 3;

    if (trackName.length > limit + threeDotsLength) {
      return `${trackName.slice(0, limit)}...`;
    }

    return trackName;
  }
}
