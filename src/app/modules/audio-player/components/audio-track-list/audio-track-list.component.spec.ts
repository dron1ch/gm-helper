import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AudioTrackListComponent } from './audio-track-list.component';
import { AudioListService } from '../../services/audio-list.service';

describe('AudioTrackListComponent', () => {
  let component: AudioTrackListComponent;
  let fixture: ComponentFixture<AudioTrackListComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        providers: [
          {
            provide: AudioListService,
            useValue: {
              trackList$: {
                // TODO: mock this adequately
                subscribe: () => null,
              },
            },
          },
        ],
        declarations: [AudioTrackListComponent],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(AudioTrackListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
