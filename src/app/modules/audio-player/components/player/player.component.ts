import { Component, OnInit, Input } from '@angular/core';
import { MatSliderChange } from '@angular/material/slider';

import { AudioListService } from '@app/modules/audio-player/services/audio-list.service';
import { STARTING_VOLUME } from '@app/modules/audio-player/utils/constants';

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.scss'],
})
export class PlayerComponent implements OnInit {
  @Input() trackIndex: number;
  @Input() repeatFeature = false;
  @Input() deleteFeature = false;
  volumeRating = STARTING_VOLUME;
  needToRepeat = false;
  isPlaying = false;
  isPaused = true;

  currentlyPlaying: string[] = [];
  constructor(public audioListService: AudioListService) {}

  ngOnInit(): void {
    this.needToRepeat = this.repeatFeature;
    this.setupTrack();
  }

  startTrack() {
    this.audioListService.startTrack(this.trackIndex);
  }

  pauseTrack() {
    this.audioListService.pauseTrack(this.trackIndex);
  }

  handleVolumeChangeOnTrack(event: MatSliderChange) {
    const normalisedVolume = event.value * 0.1;
    this.audioListService.changeVolumeOnTrack(
      this.trackIndex,
      normalisedVolume
    );
  }

  formatLabel(value: number) {
    return Math.trunc(value);
  }

  deleteTrack() {
    this.audioListService.removeTrackFromList(this.trackIndex);
  }

  switchRepeatForTrack() {
    this.needToRepeat = !this.needToRepeat;
    this.audioListService.setRepeatForTrack(this.trackIndex, this.needToRepeat);
  }

  private setupTrack() {
    if (this.needToRepeat) {
      this.audioListService.setRepeatForTrack(
        this.trackIndex,
        this.needToRepeat
      );
    }

    const trackToSetup = this.audioListService.getTrackList()[this.trackIndex];

    trackToSetup.howl
      .on('end', () => {
        this.isPaused = true;
        this.isPlaying = false;
      })
      .on('play', () => {
        this.isPaused = false;
        this.isPlaying = true;
      })
      .on('pause', () => {
        this.isPaused = true;
        this.isPlaying = false;
      })
      .on('volume', () => {
        const howlVolume = trackToSetup.howl.volume();
        const normalisedVolume =
          howlVolume === 1 ? howlVolume : howlVolume / 0.1;
        this.volumeRating = normalisedVolume;
      });

    this.audioListService.updateTrack(this.trackIndex, trackToSetup);
  }
}
