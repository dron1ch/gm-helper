import { TestBed, waitForAsync } from '@angular/core/testing';

import { PlayerComponent } from './player.component';
import { AudioListService } from '../../services/audio-list.service';
import { Howl } from 'howler';

describe('PlayerComponent', () => {
  // let component: PlayerComponent;
  // let fixture: ComponentFixture<PlayerComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      providers: [
        {
          provide: AudioListService,
          useValue: {
            // TODO: mock this adequately
            getTrackList: () => [
              {
                howl: new Howl({
                  mute: true,
                  src: '',
                }),
              },
            ],
          },
        },
      ],
      declarations: [PlayerComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    // fixture = TestBed.createComponent(PlayerComponent);
    // component = fixture.componentInstance;
    // component.trackIndex = 0;
    // fixture.detectChanges();
  });

  it('should create', () => {
    // TODO: fix this test
    expect(true).toBeTruthy();
  });
});
