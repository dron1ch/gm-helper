import { Component, OnInit } from '@angular/core';

import { MatSliderChange } from '@angular/material/slider';

import { PlayerComponent } from '@app/modules/audio-player/components/player/player.component';
import { AudioListService } from '@app/modules/audio-player/services/audio-list.service';

@Component({
  selector: 'app-group-player',
  templateUrl: './group-player.component.html',
  styleUrls: ['./group-player.component.scss'],
})
export class GroupPlayerComponent extends PlayerComponent implements OnInit {
  hasCheckedTracks = false;
  constructor(audioListService: AudioListService) {
    super(audioListService);
  }

  ngOnInit() {
    this.audioListService.trackList$.subscribe((trackList) => {
      if (trackList.length) {
        this.hasCheckedTracks = this.checkIfThereAreCheckedTracks();
      }
    });
  }

  startCheckedTracks() {
    const hasCheckedTracks = this.checkIfThereAreCheckedTracks();

    if (hasCheckedTracks) {
      this.isPaused = false;
      this.isPlaying = true;
      this.audioListService.startCheckedTracks();
    }
  }

  pauseCheckedTracks() {
    const hasCheckedTracks = this.checkIfThereAreCheckedTracks();

    if (hasCheckedTracks) {
      this.isPaused = true;
      this.isPlaying = false;
      this.audioListService.pauseCheckedTracks();
    }
  }

  checkIfThereAreCheckedTracks() {
    return this.audioListService.getTrackList().some((track) => track.checked);
  }

  changeVolumeOnCheckedTracks(event: MatSliderChange) {
    const hasCheckedTracks = this.checkIfThereAreCheckedTracks();
    if (hasCheckedTracks) {
      const normalisedVolume = event.value * 0.1;

      this.audioListService.changeVolumeOnCheckedTracks(normalisedVolume);
    }
  }
}
