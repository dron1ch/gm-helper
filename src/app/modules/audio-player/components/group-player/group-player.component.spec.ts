import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { GroupPlayerComponent } from './group-player.component';
import { AudioListService } from '../../services/audio-list.service';

describe('GroupPlayerComponent', () => {
  let component: GroupPlayerComponent;
  let fixture: ComponentFixture<GroupPlayerComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      providers: [
        {
          provide: AudioListService,
          useValue: {
            // TODO: mock this adequately
            trackList$: {
              subscribe: () => null,
            },
          },
        },
      ],
      declarations: [GroupPlayerComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupPlayerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
