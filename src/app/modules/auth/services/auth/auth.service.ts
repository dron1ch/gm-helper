import { from } from 'rxjs';
import { tap } from 'rxjs/operators';
import { User } from 'firebase';
import firebase from 'firebase/app';
import 'firebase/auth';

import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { ErrorsService } from '@app/services/errors/errors.service';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  user: User;

  constructor(public router: Router, private errorsService: ErrorsService) {}

  logIn(email: string, password: string) {
    try {
      const loginObservable$ = from(
        firebase.auth().signInWithEmailAndPassword(email, password)
      ).pipe(
        tap((ev) => {
          this.user = ev.user;
        })
      );

      return loginObservable$;
    } catch (error) {
      this.errorsService.emitError(error);
    }
  }

  registerNewUser(email: string, password: string) {
    try {
      const registerObservable$ = from(
        firebase.auth().createUserWithEmailAndPassword(email, password)
      );

      return registerObservable$;
    } catch (error) {
      this.errorsService.emitError(error);
    }
  }

  recoverPassword(email: string) {
    try {
      const recoverPasswordObservable$ = from(
        firebase.auth().sendPasswordResetEmail(email)
      );

      return recoverPasswordObservable$;
    } catch (error) {
      this.errorsService.emitError(error);
    }
  }

  logOut() {
    try {
      from(firebase.auth().signOut()).subscribe();
    } catch (error) {
      this.errorsService.emitError(error);
    }
  }
}
