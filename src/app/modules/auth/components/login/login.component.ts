import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '@app/modules/auth/services/auth/auth.service';
import { ErrorsService } from '@app/services/errors/errors.service';
import { LocalStorageService } from '@src/app/services/local-storage/local-storage.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent {
  isLoading = false;
  email: '';
  password: '';

  constructor(
    private authService: AuthService,
    private errorsService: ErrorsService,
    private localStorageService: LocalStorageService,
    private router: Router
  ) {}

  handleLogin() {
    this.isLoading = true;
    const loginObservable$ = this.authService.logIn(this.email, this.password);

    if (loginObservable$) {
      loginObservable$.subscribe(
        (data) => {
          const uid = data.user.uid;
          this.localStorageService.setItem('user', uid).subscribe(() => {
            this.isLoading = false;
            this.router.navigate([`/main/settings`]);
          });
        },
        (error) => {
          this.errorsService.emitError(error);
          this.isLoading = false;
        }
      );
    } else {
      this.isLoading = false;
    }
  }
}
