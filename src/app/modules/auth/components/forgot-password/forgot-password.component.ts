import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '@app/modules/auth/services/auth/auth.service';
import { ErrorsService } from '@app/services/errors/errors.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss'],
})
export class ForgotPasswordComponent {
  isLoading = false;
  email = '';

  constructor(
    private authService: AuthService,
    private errorsService: ErrorsService,
    private router: Router
  ) {}

  recoverPassword() {
    this.isLoading = true;
    const recoverPasswordObservable$ = this.authService.recoverPassword(
      this.email
    );

    if (recoverPasswordObservable$) {
      recoverPasswordObservable$.subscribe(
        () => {
          this.isLoading = false;
          this.router.navigate([`/auth/login`]);
        },
        (error) => {
          this.errorsService.emitError(error);
          this.isLoading = false;
        }
      );
    } else {
      this.isLoading = false;
    }
  }
}
