import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { AuthService } from '@app/modules/auth/services/auth/auth.service';
import { LocalStorageService } from '@app/services/local-storage/local-storage.service';
import { ErrorsService } from '@app/services/errors/errors.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {
  emailFormGroup: FormGroup;
  passwordFormGroup: FormGroup;
  isLoading = false;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private localStorageService: LocalStorageService,
    private errorsService: ErrorsService,
    private router: Router
  ) {}

  ngOnInit() {
    this.emailFormGroup = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
    });
    this.passwordFormGroup = this.formBuilder.group({
      password: ['', [Validators.required, Validators.minLength(8)]],
    });
  }

  registerNewUser() {
    const { email } = this.emailFormGroup.value;
    const { password } = this.passwordFormGroup.value;

    this.isLoading = true;
    this.authService.registerNewUser(email, password).subscribe(
      (data) => {
        const uid = data.user.uid;
        this.localStorageService.setItem('user', uid).subscribe(() => {
          this.isLoading = false;
          this.router.navigate([`/main/settings`]);
        });
      },
      (error) => {
        this.errorsService.emitError(error);
        this.isLoading = false;
      }
    );
  }
}
