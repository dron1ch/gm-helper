import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

type Note = {
  content: string;
  isChecked: boolean;
};

@Component({
  selector: 'app-session-plan',
  templateUrl: './session-plan-dialog.component.html',
  styleUrls: ['./session-plan-dialog.component.scss'],
})
export class SessionPlanComponent {
  notes: Note[] = [];
  noteEditorActive = false;
  constructor(
    public dialogRef: MatDialogRef<SessionPlanComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  addNewNote() {
    this.noteEditorActive = true;
  }

  removeNote(index: number) {
    this.notes.splice(index, 1);
  }

  handleSelectionChange(index: number) {
    this.notes[index].isChecked = !this.notes[index].isChecked;
  }

  handleEditorEmit(data: string) {
    this.notes.push({
      content: data,
      isChecked: false,
    });

    this.noteEditorActive = false;
  }
}
