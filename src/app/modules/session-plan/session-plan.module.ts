import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatListModule } from '@angular/material/list';
import { MatCheckboxModule } from '@angular/material/checkbox';

import { SessionPlanComponent } from '@src/app/modules/session-plan/components/session-plan-dialog/session-plan-dialog.component';
import { MatButtonModule } from '@angular/material/button';
import { QuillModule } from '../quill/quill.module';

@NgModule({
  declarations: [SessionPlanComponent],
  imports: [
    CommonModule,
    FormsModule,
    MatDialogModule,
    MatFormFieldModule,
    MatListModule,
    MatButtonModule,
    MatCheckboxModule,
    QuillModule,
  ],
  exports: [SessionPlanComponent],
})
export class SessionPlanModule {}
