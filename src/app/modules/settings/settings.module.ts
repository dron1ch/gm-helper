import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from '@app/app-routing.module';

import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatDividerModule } from '@angular/material/divider';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

import { SettingsListComponent } from '@app/modules/settings/components/settings-list/settings-list.component';
import { SettingCreationDialogComponent } from '@app/modules/settings/components/setting-creation-dialog/setting-creation-dialog.component';
import { SettingsDataService } from '@app/modules/settings/services/settings-data.service';
import { SettingComponent } from '@app/modules/settings/components/setting/setting.component';
import { MapsDataService } from '@app/modules/maps/services/maps-data-service/maps-data.service';
import { NpcsDataService } from '@app/modules/npcs/services/npcs-data-service/npcs-data.service';
import { CampaignsModule } from '@app/modules/campaigns/campaigns.module';
import { MapsModule } from '@app/modules/maps/maps.module';
import { LocationsModule } from '@app/modules/locations/locations.module';
import { NpcsModule } from '@app/modules/npcs/npcs.module';

@NgModule({
  declarations: [
    SettingsListComponent,
    SettingCreationDialogComponent,
    SettingComponent,
  ],
  imports: [
    CommonModule,
    AppRoutingModule,
    FormsModule,
    MatIconModule,
    MatDialogModule,
    MatFormFieldModule,
    MatButtonModule,
    MatInputModule,
    MatTooltipModule,
    MatListModule,
    MatDividerModule,
    MatProgressSpinnerModule,
    CampaignsModule,
    MapsModule,
    LocationsModule,
    NpcsModule,
  ],
  providers: [SettingsDataService, MapsDataService, NpcsDataService],
  exports: [SettingsListComponent],
})
export class SettingsModule {}
