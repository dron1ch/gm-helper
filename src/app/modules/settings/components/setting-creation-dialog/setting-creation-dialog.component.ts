import { Component, OnInit, Inject } from '@angular/core';

import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

import { SETTING_NAME_STUB } from '@app/modules/settings/utils/constants';

export type SettingCreationData = {
  name: string;
};

@Component({
  selector: 'app-setting-creation-dialog',
  templateUrl: './setting-creation-dialog.component.html',
  styleUrls: ['./setting-creation-dialog.component.scss'],
})
export class SettingCreationDialogComponent implements OnInit {
  placeholder = SETTING_NAME_STUB;
  constructor(
    public dialogRef: MatDialogRef<SettingCreationDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: SettingCreationData
  ) {}

  ngOnInit() {
    if (!this.data.name) {
      this.data.name = this.placeholder;
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
