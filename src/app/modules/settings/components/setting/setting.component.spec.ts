import { RouterModule } from '@angular/router';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MatDialogModule } from '@angular/material/dialog';

import { SettingComponent } from '@app/modules/settings/components/setting/setting.component';

describe('SettingComponent', () => {
  let component: SettingComponent;
  let fixture: ComponentFixture<SettingComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        imports: [
          RouterModule.forRoot([], { relativeLinkResolution: 'legacy' }),
          HttpClientTestingModule,
          MatDialogModule,
        ],
        declarations: [SettingComponent],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(true).toBeTruthy();
  });
});
