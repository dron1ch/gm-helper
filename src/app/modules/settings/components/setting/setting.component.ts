import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { MatDialog } from '@angular/material/dialog';

import { SettingsDataService } from '@app/modules/settings/services/settings-data.service';
import { Setting } from '@app/modules/settings/types/settings';
import { Npc as SettingNpc } from '@app/modules/npcs/types/npc';
import { Location as SettingLocation } from '@app/modules/locations/types/location';

@Component({
  selector: 'app-setting',
  templateUrl: './setting.component.html',
  styleUrls: ['./setting.component.scss'],
})
export class SettingComponent implements OnInit {
  isLoading: boolean;
  setting: Setting;
  settingNpcs: SettingNpc[] = [];
  settingLocations: SettingLocation[] = [];

  constructor(
    private route: ActivatedRoute,
    private settingsDataService: SettingsDataService,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {
    const settingId = parseInt(
      this.route.snapshot.paramMap.get('settingId'),
      10
    );

    this.isLoading = true;

    this.settingsDataService.getSetting(settingId).subscribe((setting) => {
      this.setting = setting;
      this.isLoading = false;
    });
  }
}
