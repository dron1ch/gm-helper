import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import { SettingsDataService } from '@app/modules/settings/services/settings-data.service';

import { Setting } from '@app/modules/settings/types/settings';
import { Setting as SettingConstructor } from '@app/modules/settings/utils/setting';
import {
  SettingCreationDialogComponent,
  SettingCreationData,
} from '@app/modules/settings/components/setting-creation-dialog/setting-creation-dialog.component';
import { AuthService } from '@src/app/modules/auth/services/auth/auth.service';

@Component({
  selector: 'app-settings-list',
  templateUrl: './settings-list.component.html',
  styleUrls: ['./settings-list.component.scss'],
})
export class SettingsListComponent implements OnInit {
  isLoading = true;
  settings: Setting[];

  constructor(
    private settingsDataService: SettingsDataService,
    private authService: AuthService,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.settingsDataService
      .listSettings({
        filter: { userId: this.authService.user.uid },
      })
      .subscribe((settings) => {
        this.isLoading = false;
        this.settings = settings;
      });
  }

  addSettingToList(setting: Setting) {
    this.settingsDataService
      .createSetting(setting)
      .subscribe((savedSetting) => {
        this.settings.push(savedSetting);
        this.isLoading = false;
      });
  }

  removeSettingFromList(settingId: number) {
    this.isLoading = true;

    // TODO: implement recursive data removal for child entities
    this.settingsDataService.deleteSetting(settingId).subscribe(() => {
      this.settings = this.settings.filter((s) => s.id !== settingId);
      this.isLoading = false;
    });
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(SettingCreationDialogComponent, {
      width: '150rem',
      data: { userId: this.authService.user.uid } as { userId: string },
    });

    dialogRef.afterClosed().subscribe((result: SettingCreationData) => {
      if (result) {
        this.isLoading = true;
        const newSetting = new SettingConstructor({
          ...result,
        });
        this.addSettingToList(newSetting);
      }
    });
  }
}
