import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { MatDialogModule } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { RouterTestingModule } from '@angular/router/testing';

import { SettingsListComponent } from './settings-list.component';
import { SettingsDataService } from '../../services/settings-data.service';
import { AuthService } from '@src/app/modules/auth/services/auth/auth.service';

describe('SettingsListComponent', () => {
  let component: SettingsListComponent;
  let fixture: ComponentFixture<SettingsListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      providers: [
        {
          provide: SettingsDataService,
          useValue: {
            listSettings: () => new Observable(),
          },
        },
        {
          provide: AuthService,
          useValue: {
            user: {
              uid: 'someuid',
            },
          },
        },
      ],
      imports: [MatDialogModule, RouterTestingModule],
      declarations: [SettingsListComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
