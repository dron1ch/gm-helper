import { Setting as TSetting } from '@app/modules/settings/types/settings';

import { SETTING_NAME_STUB } from '@app/modules/settings/utils/constants';
import { generateId } from '@app/utils/generate-id/generate-id';

export class Setting implements TSetting {
  id = generateId();
  name = SETTING_NAME_STUB;
  campaignsIds?: number[] = [];
  mapsIds?: number[] = [];
  npcsIds?: number[] = [];
  userId: string;

  constructor(options?: Partial<TSetting>) {
    if (options) {
      const { name, userId } = options;

      this.name = name ?? this.name;
      this.userId = userId;
    }
  }
}
