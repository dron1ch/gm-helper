export type Setting = {
  id: number;
  name: string;
  userId: string;
};
