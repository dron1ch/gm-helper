import { Injectable } from '@angular/core';

import { Setting } from '@app/modules/settings/types/settings';
import { DataService, LocalStorageKeys } from '@app/services/data/data.service';
import { SETTINGS_URL } from '@app/modules/settings/utils/constants';

@Injectable({
  providedIn: 'root',
})
export class SettingsDataService extends DataService<Setting> {
  dataUrl = SETTINGS_URL;
  localStorageKey = LocalStorageKeys.Settings;

  listSettings = this.listEntities;
  getSetting = this.getEntity;
  createSetting = this.createEntity;
  updateSetting = this.updateEntity;
  deleteSetting = this.deleteEntity;
}
