import { TestBed } from '@angular/core/testing';

import { SettingsDataService } from './settings-data.service';

describe('SettingsDataService', () => {
  let service: SettingsDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {
          provide: SettingsDataService,
          useValue: {},
        },
      ],
    });
    service = TestBed.inject(SettingsDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
