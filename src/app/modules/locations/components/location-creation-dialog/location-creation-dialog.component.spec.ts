import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatAutocompleteModule } from '@angular/material/autocomplete';

import { LocationCreationDialogComponent } from './location-creation-dialog.component';
import { Observable } from 'rxjs';
import { LocationsDataService } from '../../services/locations-data/locations-data.service';

describe('LocationCreationDialogComponent', () => {
  let component: LocationCreationDialogComponent;
  let fixture: ComponentFixture<LocationCreationDialogComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [MatAutocompleteModule],
      providers: [
        {
          provide: MatDialogRef,
          useValue: {},
        },
        {
          provide: MAT_DIALOG_DATA,
          useValue: {},
        },
        {
          provide: LocationsDataService,
          useValue: {
            listLocations: () => new Observable(),
          },
        },
      ],
      declarations: [LocationCreationDialogComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LocationCreationDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
