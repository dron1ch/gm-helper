import { Observable } from 'rxjs';
import { Component, OnInit, Inject } from '@angular/core';
import { MatOption } from '@angular/material/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { Location } from '@app/modules/locations/types/location';
import { Location as LocationConstructor } from '@app/modules/locations/utils/location';
import { LocationsDataService } from '@app/modules/locations/services/locations-data/locations-data.service';

@Component({
  selector: 'app-marker-creation-dialog',
  templateUrl: './location-creation-dialog.component.html',
  styleUrls: ['./location-creation-dialog.component.scss'],
})
export class LocationCreationDialogComponent implements OnInit {
  detailedLocationEditorOpened: boolean;
  locations$: Observable<Location[]> = null;
  parentLocationName = '';

  constructor(
    private locationsDataService: LocationsDataService,
    public dialogRef: MatDialogRef<LocationCreationDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Location
  ) {}

  ngOnInit(): void {
    this.data = new LocationConstructor(this.data);
    this.locations$ = this.locationsDataService.listLocations({
      filter: {
        settingId: this.data.settingId,
      },
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  handleChange(selectedOption: MatOption) {
    this.parentLocationName = selectedOption.value.name;
    this.data = {
      ...this.data,
      parentLocationId: selectedOption.value.id,
    };
  }

  handleEditorEmit(data: string) {
    this.data = {
      ...this.data,
      detailedLocationInfo: data,
    };

    this.closeDetailedLocationEditor();
  }

  mapLocationDataToName(location: Location) {
    return location ? location.name : null;
  }

  openDetailedLocationEditor() {
    this.detailedLocationEditorOpened = true;
  }

  closeDetailedLocationEditor() {
    this.detailedLocationEditorOpened = false;
  }
}
