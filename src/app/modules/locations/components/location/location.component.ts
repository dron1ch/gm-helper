import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { LocationsDataService } from '@app/modules/locations/services/locations-data/locations-data.service';
import { Location } from '@app/modules/locations/types/location';

@Component({
  selector: 'app-location',
  templateUrl: './location.component.html',
  styleUrls: ['./location.component.scss'],
})
export class LocationComponent implements OnInit {
  location: Location;
  isLoading = false;
  locationEditorOpened = false;

  constructor(
    private route: ActivatedRoute,
    private locationsDataService: LocationsDataService
  ) {}

  ngOnInit(): void {
    const locationId = parseInt(
      this.route.snapshot.paramMap.get('locationId'),
      10
    );
    this.locationsDataService
      .getLocation(locationId)
      .subscribe((locationRsult) => {
        this.location = locationRsult;
      });
  }

  openLocationEditor() {
    this.locationEditorOpened = true;
  }

  handleEditorEmit(detailedLocationInfo: string) {
    this.isLoading = true;
    this.saveLocation({
      ...this.location,
      detailedLocationInfo,
    }).subscribe({
      next: (location) => {
        this.isLoading = false;
        this.locationEditorOpened = false;
        this.location = location;
      },
    });
  }

  private saveLocation(location: Location) {
    return this.locationsDataService.updateLocation(location.id, location);
  }
}
