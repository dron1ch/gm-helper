import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { LocationComponent } from './location.component';
import { LocationsDataService } from '../../services/locations-data/locations-data.service';
import { Observable } from 'rxjs';
import { RouterModule } from '@angular/router';

describe('LocationComponent', () => {
  let component: LocationComponent;
  let fixture: ComponentFixture<LocationComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        providers: [
          {
            provide: LocationsDataService,
            useValue: {
              getLocation: () => new Observable(),
            },
          },
        ],
        imports: [
          RouterModule.forRoot([], { relativeLinkResolution: 'legacy' }),
        ],
        declarations: [LocationComponent],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(LocationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
