import { Component, OnInit, Input } from '@angular/core';

import { MatDialog } from '@angular/material/dialog';

import { AuthService } from '@app/modules/auth/services/auth/auth.service';
import { Location } from '@app/modules/locations/types/location';
import { LocationsDataService } from '@app/modules/locations/services/locations-data/locations-data.service';
import { LocationCreationDialogComponent } from '@app/modules/locations/components/location-creation-dialog/location-creation-dialog.component';
import { validateLocation } from '@app/modules/locations/utils/validation';
import { ErrorsService } from '@src/app/services/errors/errors.service';

@Component({
  selector: 'app-locations-list',
  templateUrl: './locations-list.component.html',
  styleUrls: ['./locations-list.component.scss'],
})
export class LocationsListComponent implements OnInit {
  @Input() settingId: number;

  isLoading: boolean;
  locations: Location[];

  constructor(
    private locationsDataService: LocationsDataService,
    private authService: AuthService,
    private errorsService: ErrorsService,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.isLoading = true;

    const user = this.authService.user;
    const filter = { settingId: this.settingId, userId: user.uid };

    this.locationsDataService
      .listLocations({
        filter,
      })
      .subscribe((locations) => {
        this.locations = locations instanceof Array ? locations : [];
        this.isLoading = false;
      });
  }

  openLocationDialog(): void {
    const user = this.authService.user;

    const dialogRef = this.dialog.open(LocationCreationDialogComponent, {
      width: '150rem',
      data: { settingId: this.settingId, userId: user.uid } as {
        settingId: number;
        userId: string;
      },
    });

    dialogRef.afterClosed().subscribe((result: Location) => {
      if (result) {
        const [isValid, error] = validateLocation(result);
        if (isValid) {
          this.addLocationToList({ ...result });
        } else {
          this.errorsService.emitError(error);
        }
      }
    });
  }

  addLocationToList(location: Location) {
    this.isLoading = true;

    this.locationsDataService
      .createLocation(location)
      .subscribe((createdLocation) => {
        this.locations.push(createdLocation);
        this.isLoading = false;
      });
  }

  removeLocationFromList(locationId: number) {
    this.isLoading = true;

    this.locationsDataService.deleteLocation(locationId).subscribe(() => {
      this.locations = this.locations.filter((l) => l.id !== locationId);
      this.isLoading = false;
    });
  }
}
