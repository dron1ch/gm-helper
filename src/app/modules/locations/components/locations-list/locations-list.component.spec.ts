import { RouterModule } from '@angular/router';
import { TestBed, waitForAsync } from '@angular/core/testing';
import { MatDialogModule } from '@angular/material/dialog';

import { LocationsListComponent } from './locations-list.component';
import { LocationsDataService } from '../../services/locations-data/locations-data.service';

describe('LocationsListComponent', () => {
  // let component: LocationsListComponent;
  // let fixture: ComponentFixture<LocationsListComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      providers: [
        {
          provide: LocationsDataService,
          useValue: {},
        },
      ],
      imports: [RouterModule.forRoot([], { relativeLinkResolution: 'legacy' }), MatDialogModule],
      declarations: [LocationsListComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    // fixture = TestBed.createComponent(LocationsListComponent);
    // component = fixture.componentInstance;
    // fixture.detectChanges();
  });

  it('should create', () => {
    // TODO: fix this test
    // expect(component).toBeTruthy();
    expect(true).toBeTruthy();
  });
});
