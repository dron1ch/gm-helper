import { Location } from '@app/modules/locations/types/location';
import { FirebaseError } from 'firebase';

export const validateLocation = (
  location: Location
): [boolean, FirebaseError | null] => {
  const requiredFields = ['id', 'name', 'locationInfo', 'settingId', 'userId'];
  const isValid =
    Object.keys(location).filter(
      (key) => requiredFields.includes(key) && location[key]
    ).length === requiredFields.length;

  if (isValid) {
    return [isValid, null];
  } else {
    const error: FirebaseError = {
      code: 'validation/some-fields-are-empty',
      message: `We expect this entity to contain the following fields: 'name', 'locationInfo'`,
      name: null,
    };
    return [isValid, error];
  }
};
