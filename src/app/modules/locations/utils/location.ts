import {
  Location as TLocation,
  LocationType,
} from '@app/modules/locations/types/location';
import {
  LOCATION_NAME_STUB,
  LOCATION_DETAILED_INFO_STUB,
  LOCATION_INFO_STUB,
} from '@app/modules/locations/utils/constants';
import { generateId } from '@app/utils/generate-id/generate-id';

export class Location implements TLocation {
  id = generateId();
  name = LOCATION_NAME_STUB;
  settingId: number;
  locationInfo = LOCATION_INFO_STUB;
  detailedLocationInfo = LOCATION_DETAILED_INFO_STUB;
  locationType: LocationType = LocationType.Default;
  parentLocationId: number = null;
  userId: string;

  constructor(options?: Partial<TLocation> & { userId: string }) {
    const {
      name,
      settingId,
      locationInfo,
      detailedLocationInfo,
      locationType,
      parentLocationId,
      userId,
    } = options;

    this.settingId = settingId ?? this.settingId;
    this.locationInfo = locationInfo ?? this.locationInfo;
    this.detailedLocationInfo =
      detailedLocationInfo ?? this.detailedLocationInfo;
    this.name = name ?? this.name;
    this.locationType = locationType ?? this.locationType;
    this.parentLocationId = parentLocationId ?? this.parentLocationId;
    this.userId = userId;
  }
}
