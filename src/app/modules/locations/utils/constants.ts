export const LOCATIONS_URL =
  'https://us-central1-gm-helper-d65e8.cloudfunctions.net/locations';
export const LOCATION_NAME_STUB = 'Hole in the ground';
export const LOCATION_INFO_STUB =
  'In a hole in the ground there lived a hobbit';
export const LOCATION_DETAILED_INFO_STUB = `<p>Not a nasty, dirty, wet hole, filled with the ends of worms and an oozy smell,
  nor yet a dry, bare, sandy hole with nothing in it to sit down on or to eat: it was a hobbit-hole, and that means comfort.</p>`;
