export type Location = {
  id: number;
  name: string;
  locationInfo: string;
  settingId: number;
  userId: string;
  detailedLocationInfo?: string;
  locationType?: LocationType;
  parentLocationId?: number;
};

export enum LocationType {
  Default = 'default',
  Area = 'area',
  Empire = 'empire',
  Kingdom = 'kingdom',
  Duchy = 'duchy',
  Barony = 'barony',
  Capital = 'capital',
  City = 'city',
  Borough = 'borough',
  Building = 'building',
  Road = 'road',
  Monument = 'monument',
  Forest = 'forest',
  Waterbody = 'waterbody',
  Elevation = 'elevation',
}
