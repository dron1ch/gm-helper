import { Injectable } from '@angular/core';
import { Location } from '@app/modules/locations/types/location';
import { DataService, LocalStorageKeys } from '@app/services/data/data.service';

import { LOCATIONS_URL } from '@app/modules/locations/utils/constants';

@Injectable({
  providedIn: 'root',
})
export class LocationsDataService extends DataService<Location> {
  dataUrl = LOCATIONS_URL;
  localStorageKey = LocalStorageKeys.Locations;

  listLocations = this.listEntities;
  getLocation = this.getEntity;
  createLocation = this.createEntity;
  updateLocation = this.updateEntity;
  deleteLocation = this.deleteEntity;
}
