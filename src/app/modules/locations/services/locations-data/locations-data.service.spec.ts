import { TestBed } from '@angular/core/testing';

import { LocationsDataService } from './locations-data.service';

describe('LocationsDataService', () => {
  let service: LocationsDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {
          provide: LocationsDataService,
          useValue: {},
        },
      ],
    });
    service = TestBed.inject(LocationsDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
