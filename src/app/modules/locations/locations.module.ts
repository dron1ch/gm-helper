import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';

import { LocationComponent } from '@app/modules/locations/components/location/location.component';
import { LocationsListComponent } from '@app/modules/locations/components/locations-list/locations-list.component';
import { LocationCreationDialogComponent } from '@app/modules/locations/components/location-creation-dialog/location-creation-dialog.component';
import { QuillModule } from '@app/modules/quill/quill.module';

@NgModule({
  declarations: [
    LocationComponent,
    LocationsListComponent,
    LocationCreationDialogComponent,
  ],
  imports: [
    CommonModule,
    MatFormFieldModule,
    FormsModule,
    MatInputModule,
    MatFormFieldModule,
    MatDialogModule,
    MatButtonModule,
    MatProgressSpinnerModule,
    MatAutocompleteModule,
    MatIconModule,
    MatListModule,
    QuillModule,
    RouterModule,
  ],
  exports: [
    LocationComponent,
    LocationsListComponent,
    LocationCreationDialogComponent,
  ],
})
export class LocationsModule {}
