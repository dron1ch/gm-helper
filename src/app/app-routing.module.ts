import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from '@app/app.component';
import { MainComponent } from '@app/components/main/main.component';
import { CampaignComponent } from '@app/modules/campaigns/components/campaign/campaign.component';
import { SettingsListComponent } from '@src/app/modules/settings/components/settings-list/settings-list.component';
import { SettingComponent } from '@app/modules/settings/components/setting/setting.component';
import { MapDisplayComponent } from '@app/modules/maps/components/map-display/map-display.component';
import { NpcComponent } from '@app/modules/npcs/components/npc/npc.component';
import { LocationComponent } from '@app/modules/locations/components/location/location.component';
import { LoginComponent } from '@app/modules/auth/components/login/login.component';
import { RegisterComponent } from '@app/modules/auth/components/register/register.component';
import { ForgotPasswordComponent } from '@app/modules/auth/components/forgot-password/forgot-password.component';
import { AuthComponent } from '@app/modules/auth/components/auth/auth.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/main/settings',
    pathMatch: 'full',
  },
  {
    path: 'auth',
    component: AuthComponent,
    children: [
      { path: 'login', component: LoginComponent },
      { path: 'register', component: RegisterComponent },
      { path: 'forgot-password', component: ForgotPasswordComponent },
    ],
  },
  {
    path: 'main',
    component: MainComponent,
    children: [
      { path: 'settings', component: SettingsListComponent },
      { path: 'campaign/:campaignId', component: CampaignComponent },
      { path: 'setting/:settingId', component: SettingComponent },
      { path: 'map/:mapId', component: MapDisplayComponent },
      { path: 'location/:locationId', component: LocationComponent },
      { path: 'npc/:npcId', component: NpcComponent },
    ],
  },
  { path: '**', component: AppComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
