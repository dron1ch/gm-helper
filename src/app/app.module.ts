import { ClickOutsideModule } from 'ng-click-outside';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { LayoutModule } from '@angular/cdk/layout';

import { MatToolbarModule } from '@angular/material/toolbar';
import { MatListModule } from '@angular/material/list';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatMenuModule } from '@angular/material/menu';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

import { AppRoutingModule } from '@app/app-routing.module';
import { AppComponent } from '@app/app.component';
import { BodyComponent } from '@app/components/layout/body/body.component';
import { HeaderComponent } from '@app/components/layout/header/header.component';
import { ErrorsComponent } from '@app/components/utils/errors/errors.component';
import { MainComponent } from '@app/components/main/main.component';

import { AudioPlayerModule } from '@app/modules/audio-player/audio-player.module';
import { CampaignsModule } from '@app/modules/campaigns/campaigns.module';
import { MapsModule } from '@app/modules/maps/maps.module';
import { SessionPlanModule } from '@app/modules/session-plan/session-plan.module';
import { SettingsModule } from '@app/modules/settings/settings.module';
import { AuthModule } from '@app/modules/auth/auth.module';

import firebase from 'firebase/app';
import { firebaseConfig } from '@src/environments/environment';

firebase.initializeApp(firebaseConfig);
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    BodyComponent,
    ErrorsComponent,
    MainComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    AuthModule,
    BrowserAnimationsModule,
    ClickOutsideModule,
    MatToolbarModule,
    MatListModule,
    MatCardModule,
    MatIconModule,
    MatButtonModule,
    MatDividerModule,
    MatDialogModule,
    MatMenuModule,
    FormsModule,
    MatInputModule,
    MatFormFieldModule,
    MatSidenavModule,
    MatTooltipModule,
    MatSnackBarModule,
    MatProgressSpinnerModule,
    AudioPlayerModule,
    CampaignsModule,
    MapsModule,
    SessionPlanModule,
    SettingsModule,
    LayoutModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
