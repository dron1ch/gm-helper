import { Component } from '@angular/core';

import { MatDialog } from '@angular/material/dialog';

import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';

import { SidenavService } from '@app/services/sidenav/sidenav.service';
import { SessionPlanComponent } from '@app/modules/session-plan/components/session-plan-dialog/session-plan-dialog.component';
import { AuthService } from '@app/modules/auth/services/auth/auth.service';

type ClassNames = 'small' | 'medium';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent {
  className: ClassNames = 'medium';

  constructor(
    public dialog: MatDialog,
    private sidenavService: SidenavService,
    private authService: AuthService,
    breakpointObserver: BreakpointObserver
  ) {
    breakpointObserver
      .observe([Breakpoints.XSmall, Breakpoints.Small])
      .subscribe((result) => {
        const { matches: doesMatch } = result;

        if (doesMatch) {
          this.activateSmallLayout();
        }
      });

    breakpointObserver
      .observe([Breakpoints.Medium, Breakpoints.Large])
      .subscribe((result) => {
        const { matches: doesMatch } = result;

        if (doesMatch) {
          this.activateMediumLayout();
        }
      });
  }

  handleMainMenuClick() {
    this.sidenavService.toggleSidenavStatus();
  }

  handleShowSessionPlanButtonClick() {
    const dialogRef = this.dialog.open(SessionPlanComponent, {
      width: '50%',
    });

    dialogRef.afterClosed().subscribe();
  }

  handleLogOut() {
    this.authService.logOut();
  }

  activateSmallLayout() {
    this.className = 'small';
  }

  activateMediumLayout() {
    this.className = 'medium';
  }
}
