import { Component, OnInit } from '@angular/core';

import { SidenavService } from '@app/services/sidenav/sidenav.service';
import { sidenavLinks } from '@app/components/layout/body/utils/sidenavLinks';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.scss'],
})
export class BodyComponent implements OnInit {
  isSidenavOpen = false;
  sidenavLinks = sidenavLinks;
  constructor(
    private sidenavService: SidenavService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.sidenavService.subscriber$.subscribe((sidenavStatus) => {
      const { isOpen } = sidenavStatus;

      this.isSidenavOpen = isOpen;
    });
  }

  handleSidenavItemClick(routerLink: string) {
    this.sidenavLinks = this.sidenavLinks.map((sidenavLink) => ({
      ...sidenavLink,
      isActive: sidenavLink.name === routerLink,
    }));
    this.router.navigate([`/${routerLink}`], { relativeTo: this.route });
  }

  handleOutsideSidenavClick(event: any) {
    if (!event.target?.attributes?.name?.value) {
      this.sidenavService.setSidenavStatus({
        isOpen: false,
      });
    }
  }
}
