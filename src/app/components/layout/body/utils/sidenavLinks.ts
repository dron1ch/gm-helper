export type SidenavLink = {
  name: string;
  isActive: boolean;
};

export const sidenavLinks: SidenavLink[] = [
  {
    name: 'settings',
    isActive: false,
  },
];
