import { Component, OnInit } from '@angular/core';

import { ErrorsService } from '@app/services/errors/errors.service';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';

@Component({
  selector: 'app-errors',
  templateUrl: './errors.component.html',
  styleUrls: ['./errors.component.scss'],
})
export class ErrorsComponent implements OnInit {
  snackBarConfig: MatSnackBarConfig = {
    duration: 5000,
    panelClass: ['mat-toolbar', 'mat-warn'],
  };
  snackBarActionText = 'CLOSE';

  constructor(
    private errorsService: ErrorsService,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.errorsService.errors$.subscribe((error) => {
      if (error) {
        const snackBarRef = this.snackBar.open(
          `The following error has occured: ${error.code}`,
          this.snackBarActionText,
          this.snackBarConfig
        );

        snackBarRef.onAction().subscribe(() => {
          snackBarRef.dismiss();
        });
      }
    });
  }
}
