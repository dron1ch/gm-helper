import { of, Observable, iif } from 'rxjs';
import { mergeMap, tap, map, catchError } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

import { LocalStorageService } from '@app/services/local-storage/local-storage.service';
import { ErrorsService } from '../errors/errors.service';

export enum LocalStorageKeys {
  Settings = 'settings',
  Campaigns = 'campaigns',
  Maps = 'maps',
  Locations = 'locations',
  Npcs = 'npcs',
}

type GetDataOptions = {
  filter?: { [key: string]: string | number };
};

@Injectable({
  providedIn: 'root',
})
export abstract class DataService<T> {
  private httpOptions = {
    // eslint-disable-next-line @typescript-eslint/naming-convention
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  };

  abstract readonly dataUrl: string;
  abstract readonly localStorageKey: string;

  constructor(
    protected http: HttpClient,
    protected localStorageService: LocalStorageService,
    protected errorsService: ErrorsService
  ) {}

  protected listEntities(options?: GetDataOptions): Observable<T[]> {
    if (options) {
      const { filter } = options;
      if (filter) {
        const normalisedFilter = Object.keys(filter).reduce(
          (acc, key) => ({
            ...acc,
            [key]:
              typeof filter[key] !== 'string' ? `${filter[key]}` : filter[key],
          }),
          {}
        );

        return this.readLocalOrRemote({
          cb: () =>
            this.http
              .get<T[]>(`${this.dataUrl}`, {
                params: new HttpParams({ fromObject: normalisedFilter }),
              })
              .pipe(
                tap((data) => {
                  this.localStorageService
                    .setItem(this.localStorageKey, data)
                    .subscribe();
                }),
                catchError((error) => {
                  this.errorsService.emitError(error);

                  return of([]);
                })
              ),

          filter: this.implementNormalisedFilterAsFunction(filter),
        }) as Observable<T[]>;
      }
    }

    // TODO: also support this case in backend functions
    return this.http.get<T[]>(this.dataUrl).pipe(
      catchError((error) => {
        this.errorsService.emitError(error);

        return of([]);
      })
    );
  }

  protected getEntity(entityId: number): Observable<T> {
    return this.readLocalOrRemote({
      entityId,
      cb: () =>
        this.http.get<T>(`${this.dataUrl}/${entityId}`).pipe(
          catchError((error) => {
            this.errorsService.emitError(error);

            return of({} as T);
          })
        ),
    }) as Observable<T>;
  }

  protected createEntity(entity: T) {
    return this.http.post<T>(this.dataUrl, entity, this.httpOptions).pipe(
      tap((data: T) => {
        this.localStorageService
          .getItem(this.localStorageKey)
          .subscribe((existingData) => {
            if (existingData) {
              this.localStorageService
                .setItem(this.localStorageKey, [...existingData, data])
                .subscribe();
            } else {
              this.localStorageService
                .setItem(this.localStorageKey, data)
                .subscribe();
            }
          });
      }),
      map((data: T) => data),
      catchError((error) => {
        this.errorsService.emitError(error);

        return of({} as T);
      })
    );
  }

  protected updateEntity(entityId: number, entity: T) {
    return this.http
      .put<T>(`${this.dataUrl}/${entityId}`, entity, this.httpOptions)
      .pipe(
        tap(() =>
          // TODO: Refactor this smelly code
          this.localStorageService
            .getItem<T>(this.localStorageKey)
            .subscribe((existingData) => {
              const updatedData = existingData.map((existingEntity: any) =>
                existingEntity.id === entityId ? entity : existingEntity
              );
              this.localStorageService
                .setItem(this.localStorageKey, updatedData)
                .subscribe();
            })
        ),
        map((data: T) => data),
        catchError((error) => {
          this.errorsService.emitError(error);

          return of({} as T);
        })
      );
  }

  protected deleteEntity(entityId: number) {
    const url = `${this.dataUrl}/${entityId}`;
    return this.http.delete(url).pipe(
      tap(() => {
        this.localStorageService
          .getItem<T>(this.localStorageKey)
          // TODO: Refactor this smelly code
          .subscribe((existingData) => {
            const updatedData = existingData.filter(
              (existingEntity: any) => existingEntity.id !== entityId
            );
            if (updatedData.length) {
              this.localStorageService
                .setItem(this.localStorageKey, updatedData)
                .subscribe();
            } else {
              this.localStorageService
                .removeItem(this.localStorageKey)
                .subscribe();
            }
          });
      }),
      catchError((error) => {
        this.errorsService.emitError(error);

        return of({} as T);
      })
    );
  }

  protected readLocalOrRemote(options: {
    cb: () => Observable<T | T[]>;
    entityId?: number;
    filter?: (val: T) => boolean;
  }): Observable<T | T[]> {
    const { entityId, filter, cb } = options;
    if (entityId) {
      return this.localStorageService
        .getItem<T & { id: number }>(this.localStorageKey)
        .pipe(
          mergeMap((lsData) =>
            iif(
              () => lsData && !!lsData.find((item) => item.id === entityId),
              lsData ? of(lsData.find((item) => item.id === entityId)) : of([]),
              cb()
            )
          )
        );
    } else {
      return this.localStorageService.getItem<T>(this.localStorageKey).pipe(
        mergeMap((lsData) => {
          const values = Object.values(lsData ? lsData : {});
          return iif(
            () => !!values.length,
            of([...values].filter(filter ? filter : () => true)),
            cb()
          );
        })
      );
    }
  }

  protected implementNormalisedFilterAsFunction(filter: GetDataOptions) {
    return (val: T) =>
      Object.keys(filter).every((key) => val[key] === filter[key]);
  }
}
