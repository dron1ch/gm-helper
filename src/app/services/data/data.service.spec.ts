import { TestBed } from '@angular/core/testing';

import { DataService } from './data.service';

describe('DataServiceService', () => {
  let service: DataService<any>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        {
          provide: DataService,
          useValue: {},
        },
      ],
    });
    service = TestBed.inject(DataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
