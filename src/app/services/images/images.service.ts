import firebase, { FirebaseError } from 'firebase/app';
import 'firebase/storage';
import { from, Observable } from 'rxjs';
import { Injectable } from '@angular/core';

export type Image = {
  imageSrc: string;
};

@Injectable({
  providedIn: 'root',
})
export class ImagesService {
  storageRef = firebase.storage().ref();
  fileSizeLimit = 5e6;

  constructor() {}

  saveImage(namespace: string, file: File): Observable<string> {
    const metadata = {
      contentType: file.type,
    };

    // This can be replaced with a firebase function, but since the api surface
    // is quiet flat,I decided to go with a native firebase solution.

    // I decided to use Promises instead of async/await to have a more concise pattern in places where I call my "backend" methods.

    if (file.size > this.fileSizeLimit) {
      const error: FirebaseError = {
        code: 'file/max-file-size-of-5mb-exceeded',
        message:
          'The maximum size of a file in storage equals 5MB. This limit was exceeded',
        name: 'max-file-size-exceeded',
      };
      throw error;
    }

    const imageUrlObservable = from(
      this.storageRef
        .child(`${namespace}/` + file.name)
        .put(file, metadata)
        .catch((error: FirebaseError) => {
          throw error;
        })
        .then((snapshot) =>
          snapshot.ref.getDownloadURL().catch((error: FirebaseError) => {
            throw error;
          })
        )
    );

    return imageUrlObservable;
  }
}
