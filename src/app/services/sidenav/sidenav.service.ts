import { Subject } from 'rxjs';
import { Injectable } from '@angular/core';

export type SidenavStatus = {
  isOpen: boolean;
};

@Injectable({
  providedIn: 'root',
})
export class SidenavService {
  observer = new Subject<SidenavStatus>();
  sidenavStatus: SidenavStatus = {
    isOpen: false,
  };

  public subscriber$ = this.observer.asObservable();

  constructor() {}

  toggleSidenavStatus() {
    this.sidenavStatus = {
      ...this.sidenavStatus,
      isOpen: !this.sidenavStatus.isOpen,
    };

    this.emitData(this.sidenavStatus);
  }

  setSidenavStatus(status: SidenavStatus) {
    this.sidenavStatus = {
      ...this.setSidenavStatus,
      ...status,
    };

    this.emitData(this.sidenavStatus);
  }

  emitData(data: SidenavStatus) {
    this.observer.next(data);
  }
}
