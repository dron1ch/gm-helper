import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { FirebaseError } from 'firebase';

@Injectable({
  providedIn: 'root',
})
export class ErrorsService {
  errors$ = new Subject<FirebaseError>();

  constructor() {}

  emitError(error: FirebaseError) {
    this.errors$.next(error);
  }
}
