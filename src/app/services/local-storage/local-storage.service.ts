import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class LocalStorageService {
  constructor() {}

  setItem(key: string, data: any) {
    return new Observable((subscriber) => {
      localStorage.setItem(key, JSON.stringify(data));
      subscriber.next();
    });
  }

  getItem<T>(key: string): Observable<T[]> {
    return new Observable((subscriber) => {
      subscriber.next(JSON.parse(localStorage.getItem(key)));
    });
  }

  removeItem(key: string) {
    return new Observable((subscriber) => {
      localStorage.removeItem(key);

      subscriber.next();
    });
  }

  clear() {
    return new Observable((subscriber) => {
      localStorage.clear();

      subscriber.next();
    });
  }
}
