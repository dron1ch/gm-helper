// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
};

export const firebaseConfig = {
  apiKey: 'AIzaSyDNypP6cLQoYoeyAqUR_tc6KiIyfwszlTY',
  authDomain: 'gm-helper-d65e8.firebaseapp.com',
  databaseURL: 'https://gm-helper-d65e8.firebaseio.com',
  projectId: 'gm-helper-d65e8',
  storageBucket: 'gm-helper-d65e8.appspot.com',
  messagingSenderId: '747784813848',
  appId: '1:747784813848:web:afd17f37402de8ccb7f26d',
  measurementId: 'G-2Q6NP6RV6M',
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
