const fs = require("fs");

const file = fs
  .readdirSync("dist/gm-helper")
  .filter((fn) => fn.startsWith("main-es2015") && fn.endsWith(".js"));

module.exports = file;
